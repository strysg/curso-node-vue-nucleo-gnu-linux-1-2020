'use strict';

const Text = require('../../general/Text');

class PostContenido extends Text {
  constructor (value, errors) {
    super('contenido', value, { required: true }, errors);
  }
}

module.exports = PostContenido;
