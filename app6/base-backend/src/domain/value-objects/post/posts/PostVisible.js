'use strict';

const Bool = require('../../general/Bool');

class PostVisible extends Bool {
  constructor (value, errors) {
    super('visible', value, { required: true }, errors);
  }
}

module.exports = PostVisible;
