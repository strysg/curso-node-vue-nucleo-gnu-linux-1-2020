'use strict';

const Text = require('../../general/Text');

class PostTitulo extends Text {
  constructor (value, errors) {
    super('titulo', value, { required: true }, errors);
  }
}

module.exports = PostTitulo;
