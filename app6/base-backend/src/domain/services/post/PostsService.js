'use strict';

const debug = require('debug')('app:service:post');
const moment = require('moment');
const Service = require('../Service');

module.exports = function postService (repositories, valueObjects, res) {
  const { PostRepository, UsuarioRepository } = repositories; // importando de repositories
  const {
    PostVisible,
    PostTitulo,
    PostContenido
  } = valueObjects;  // validadores

  async function findAll(params = {}) {
    debug('Obteniendo Posts');
    let lista;

    try {
      lista = await PostRepository.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de posts`));
    }

    return res.success(lista);
  };

  async function findById(id) {
    debug('Buscar Post por id');
    let post;
    try {
      post = await PostRepository.findById(id);
    } catch (e) {
      return res.error(e);
    }

    return res.success(post);
  };

  async function createOrUpdate(data) {
    debug('Crear o actualizar post');

    validate(data);
    // verificando la cantidad de posts no aprobados
    if (!data.id) {
      try {
        let postsNoAprobados = await PostRepository.findAll({ visible: false, id_usuario_autor: data.id_usuario_autor });
        console.log('no aprobados:::', postsNoAprobados);
        if(postsNoAprobados.count > 3) {
          return res.error(`No se pueden crear mas de 3 posts sin aprobacion`);
        }
      } catch (e) {
        return res.error(`Error creando o actualizando post: ${e}`);
      }
    } else {
      delete data.id_usuario_autor;
    }
    // la funcion Service.createOrUpdate() llama al repository indicado y devuelve error o exito en formato
    return Service.createOrUpdate(data, PostRepository, res, 'Post');
  };

  async function deleteItem(id) {
    debug('Eliminar post');

    return Service.deleteItem(id, PostRepository, res, 'Post');
  }

  // fucion de validacion
  function validate(data) {
    Service.validate(data, {
      visible: PostVisible,
      titulo: PostTitulo,
      contenido: PostContenido
    });
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
