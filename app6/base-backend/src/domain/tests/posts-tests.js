'use strict';

const test = require('ava');
const { errors, config } = require('../../common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('PostsService#findAll', async t => {
  const { PostsService } = services;
  let res = await PostsService.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count == 2, 'Se tienen 2 posts');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('PostsService#findById', async t => {
  const { PostsService } = services;
  const id = 1;
  let res = await PostsService.findById(id);
  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('PostsService#createOrUpdate', async t => {
  const { PostsService } = services;

  const nuevoPost = {
    visible: true,
    titulo: 'Test post.',
    contenido: 'test content',
    id_usuario_autor: 2,
    id_usuario_revisor: 1, // usuario ciudadano (ver 0004-seeder-usuarios.js)
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await PostsService.createOrUpdate(nuevoPost);
  let post = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof post.id === 'number', 'Comprobando que el nuevo post tenga un id');
  t.is(post.titulo, nuevoPost.titulo, 'Creando registro - titulo');
  t.is(post.visible, nuevoPost.visible, 'Creando registro - visible');
  t.is(post.contenido, nuevoPost.contenido, 'Creando registro - contenido');
  t.is(post.id_usuario_autor, nuevoPost.id_usuario_autor, 'Creando registro - id_usuario_autor');
  t.is(res.message, 'OK', 'Mensaje correcto');

  test.idPost = post.id;
});

test.serial('PostsService#createOrUpdate - update', async t => {
  const { PostsService } = services;
  const newData = {
    id: test.idPost,
    titulo: 'Nuevo Titulo cambiado test',
    visible: false
  };

  let res = await PostsService.createOrUpdate(newData);
  let post = res.data;
  t.is(res.code, 1, 'Respuesta correcta');
  t.is(post.id, newData.id, 'Actualizando registro post - id');
  t.is(post.titulo, newData.titulo, 'Actualizando registro post - titulo');
  t.is(post.visible, newData.visible, 'Actualizando registro post - visible');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('PostsService#delete', async t => {
  const { PostsService } = services;
  let res = await PostsService.deleteItem(test.idPost);
  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Post eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('PostsService#createOrUpdate - crear mas de 3 posts no aprobados', async t => {
  const { PostsService } = services;

  const newData = [
    {
      visible: false,
      titulo: 'test post1',
      contenido: 'test content1',
      id_usuario_autor: 2,
      id_usuario_revisor: 1,
      _user_created: 1,
      _created_at: new Date()
    },
    {
      visible: false,
      titulo: 'test post2',
      contenido: 'test content2',
      id_usuario_autor: 2,
      id_usuario_revisor: 1,
      _user_created: 1,
      _created_at: new Date()
    },
    {
      visible: false,
      titulo: 'test post3',
      contenido: 'test content3',
      id_usuario_autor: 2,
      id_usuario_revisor: 1,
      _user_created: 1,
      _created_at: new Date()
    }
  ];
  let ids = [];
  // creando tres registros
  for (let i = 0; i < newData.length; i++) {
    let res = await PostsService.createOrUpdate(newData[i]);
    if (res.data.id) {
      ids.push(res.data.id);
    }
  }
  // tratando de crear un nuevo registro
  let newData4 = {
    visible: false,
    titulo: 'test post4',
    contenido: 'test content4',
    id_usuario_autor: 2,
    id_usuario_revisor: 1,
    _user_created: 1,
    _created_at: new Date()
  };
  let res = await PostsService.createOrUpdate(newData4);
  let post = res.data;

  t.is(res.code, -1, 'No se ha creado respuesta correcta');
  t.true(res.message != 'OK', 'No se ha creado');

  // borrando los registros creados de prueba
  for (let i = 0; i < ids.length; i++) {
    let res = await PostsService.deleteItem(ids[i]);
    t.is(res.code, 1, 'Respuesta correcta');
    t.true(res.data, 'Post eliminado');
    t.is(res.message, 'OK', 'Mensaje correcto');
  }
});
