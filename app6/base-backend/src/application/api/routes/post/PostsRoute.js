'use strict';

const guard = require('express-jwt-permissions')();

module.exports = function setupPost (api, controllers) {
  const { PostsController } = controllers;
  // Los siguientes endpoints todos empiezan con /post/ (por el nombre de la carpeta en este mismo directorio)
  api.post('/crear', guard.check(['posts:create']), PostsController.crearPost);
  api.get('/listarPostsAdmin', guard.check(['posts:create', 'posts:read', 'posts:update']), PostsController.listarPostsAdmin);
  api.get('/listarPostsUsuario', guard.check(['posts:read']), PostsController.listarPostsUsuario);
  api.post('/cambiarVisiblidad/:id', guard.check(['posts:update']), PostsController.cambiarVisibilidadPost);

  return api;
};
