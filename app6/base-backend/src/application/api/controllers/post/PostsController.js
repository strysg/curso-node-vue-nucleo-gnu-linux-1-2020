'use strict';

const debug = require('debug')('app:controller:post');
const { userData } = require('../../../lib/auth');
const moment = require('moment');

module.exports = function setupUsuarioController (services) {
  const { PostsService, UsuarioService } = services;

  async function crearPost (req, res, next) {
    debug('Creando post');
    const { titulo, contenido } = req.body;
    try {
      let _user = await userData(req, services);
      let respuesta = await PostsService.createOrUpdate({
        titulo,
        contenido,
        id_usuario_autor: _user.id,
        _user_created: _user.id,
        visible: false
      });
      return res.send(respuesta);
    } catch (e) {
      // usando next() de express y retornando error
      return next(e);
    }
  }

  async function listarPostsAdmin (req, res, next) {
    // un usuario admin puede ver todos los posts
    debug('listar posts');

    try {
      let respuesta = await PostsService.findAll();
      return res.send(respuesta);
    } catch (e) {
      return next(e);
    }
  }

  async function listarPostsUsuario (req, res, next) {
    // un usuario normal solo puede ver sus posts
    try {
      // comprobando usuario
      let _user = await userData(req, services);
      if (_user.id) {
        // consultando los posts solamente de ese usuario
        let respuesta = await PostsService.findAll({ id_usuario_autor: _user.id });
        return res.send(respuesta);
      }
    } catch(e) {
      return next(e);
    }
  }

  async function cambiarVisibilidadPost (req, res, next) {
    // se supone que esta funcion solo la puede llamar un usuario admin
    try {
      const { id } = req.params;
      const { visible } = req.body;
      let _user = await userData(req, services);
      let respuesta = await PostsService.createOrUpdate(
        {
          id: id, visible: visible,
          id_usuario_autor: _user.id
        });
      return res.send(respuesta);
    } catch (e) {
      return next(e);
    }
  };

  return {
    crearPost,
    listarPostsAdmin,
    listarPostsUsuario,
    cambiarVisibilidadPost,
  };
};
