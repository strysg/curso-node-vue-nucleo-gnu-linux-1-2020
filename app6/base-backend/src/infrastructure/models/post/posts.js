'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      xlabel: lang.t('fields.create'),
      defaultValue: false
    },
    titulo: {
      type: DataTypes.STRING(500),
      allowNull: false,
      defaultValue: '-'
    },
    contenido: {
      type: DataTypes.STRING(1800),
      allowNull: false,
      defaultValue: '-'
    },
  };

  // Agregando campos para el log (esto agrega los campos _user_created _user_updated _created_at _updated_at)
  fields = util.setTimestamps(fields);

  let Posts = sequelize.define('posts', fields, {
    timestamps: false,
    tableName: 'post_posts'
  });

  return Posts;
};
