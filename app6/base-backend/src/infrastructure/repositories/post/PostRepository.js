'use strict';

const { getQuery, toJSON } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function postRepository (models, Sequelize) {
  const { usuarios, modulos, posts } = models;

  async function findAll (params = {}) {
    let query = getQuery(params);

    query.where = {};
    // para incluir poder hacer consultas por campos de otras tablas
    query.include = [
      // de la tabla usuario
      {
        attributes: ['usuario', 'cargo', 'estado'],
        model: usuarios,
        as: 'usuario_autor'
      },
      { 
        attributes: ['usuario', 'cargo', 'estado'],
        model: usuarios,
        as: 'usuario_revisor'
      },
    ];

    // si se consulta por modulo
    if (params.id_modulo) {
      query.where.id_modulo = params.id_modulo;
    }

    if (params.visible !== undefined) {
      query.where.visible = params.visible;
    }

    // si se consulta por usuarios
    if (params.id_usuario_autor) {
      query.where.id_usuario_autor = params.id_usuario_autor;
    }
    if (params.id_usuario_revisor) {
      query.where.id_usuario_autor = params.id_usuario_revisor;
    }

    // ejecuta la funcion del ORM sequelize findAndCountAll
    const result = await posts.findAndCountAll(query);
    return toJSON(result);
  }

  async function findById (id) {
    const result = await posts.findOne({
      where: {
        id
      }
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, posts, t),
    deleteItem: (id, t) => Repository.deleteItem(id, posts, t)
  };
};
