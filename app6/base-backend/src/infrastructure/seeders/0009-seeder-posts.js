'use strict';

const casual = require('casual');
const { setTimestampsSeeder } = require('../lib/util');
const { text } = require('../../common');

// nuevos registros
let items = [
  {
    visible: true,
    titulo: 'Post por defecto 1',
    contenido: 'Prueba de guarado de **post** usando seeders',
    id_usuario_autor: 2, // usuario ciudadano (ver 0004-seeder-usuarios.js)
    id_usuario_revisor: 1, // usuario ciudadano (ver 0004-seeder-usuarios.js)
  },
  {
    visible: false,
    titulo: 'Post por defecto 2',
    contenido: 'Prueba 2 de guarado de **post** usando seeders 0009-seeder-posts.js',
    id_usuario_autor: 2,
  }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('post_posts', items, {});
  },

  down (queryInterface, Sequelize) { }
};
