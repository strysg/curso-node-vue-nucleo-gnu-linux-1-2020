'use strict';

const test = require('ava');
const { config, errors } = require('../../common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Post#findAll', async t => {
  // probando la lista de posts
  const { PostRepository } = repositories;
  let lista = await PostRepository.findAll();

  t.true(lista.count == 2, 'Se tienen dos posts');
});

test.serial('Post#findById', async t => {
  const { PostRepository } = repositories;
  const id = 1;

  let post = await PostRepository.findById(id);

  t.is(post.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('Post#createOrUpdate - new', async t => {
  const { PostRepository } = repositories;
  
  const nuevoPost = {
    visible: true,
    titulo: 'Test post.',
    contenido: 'test content',
    id_usuario_autor: 2,
    id_usuario_revisor: 1, // usuario ciudadano (ver 0004-seeder-usuarios.js)
    _user_created: 1,
    _created_at: new Date()
  };

  let post = await PostRepository.createOrUpdate(nuevoPost);

  t.true(typeof post.id === 'number', 'Comprobando que el post tenga un id');
  t.is(post.titulo, nuevoPost.titulo, 'Creando registro - titulo');
  t.is(post.contenido, nuevoPost.contenido, 'Creando registro - contenido');
  t.is(post.visible, nuevoPost.visible, 'Creando registro - visible');

  test.idPost = post.id;
});

test.serial('Post#createOrUpdate - update', async t => {
  const { PostRepository } = repositories;
  
  const newData = {
    id: test.idPost,
    titulo: 'Cambiando titulo',
    contenido: 'Nuevo contenido de test',
  };

  let post = await PostRepository.createOrUpdate(newData);

  t.is(post.titulo, newData.titulo, 'Modificando registro - titulo');
  t.is(post.contenido, newData.contenido, 'Modificando registro - contenido');
});

test.serial('Post#findAll#filter#titulo', async t => {
  const { PostRepository } = repositories;
  let lista = await PostRepository.findAll({ titulo: 'Cambiando titulo' });

  t.true(lista.count >= 1, 'Se tiene 1 registros en la bd');
});

test.serial('Post#findAll#filter#visible', async t => {
  const { PostRepository } = repositories;
  let lista = await PostRepository.findAll({ visible: true });

  t.true(lista.count >= 2, 'Se tiene al menos 2 registros en la bd');
});

test.serial('Post#delete', async t => {
  const { PostRepository } = repositories;
  let deleted = await PostRepository.deleteItem(test.idPost);

  t.is(deleted, 1, 'Post eliminado');
});
