'use strict';

const debug = require('debug')('app:db');

const db = {
  database: process.env.DB_NAME || 'base-backend',
  username: process.env.DB_USER || 'developer',
  password: process.env.DB_PASS || 'developer1',
  host: process.env.DB_HOST || 'localhost',
  dialect: 'postgres',
  timezone: 'America/La_Paz',
  logging: s => debug(s)
};

module.exports = db;
