'use strict';
// Basado en: https://www.twilio.com/blog/2017/08/http-requests-in-node-js.html
var https = require('https');

const doRequest = (url, n) => {
  console.log(`--> (${n}) Petición a: ${url}`);
  https.get(url, (resp) => {
    let data = '';
    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      console.log(`<-- (${n}) Respuesta total ${url}: ${data.length}`);
    });

  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });
};

module.exports = {
  doRequest
};
