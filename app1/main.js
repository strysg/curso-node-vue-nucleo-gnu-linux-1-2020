'use strict';

const request1 = require('./request1');

function mostrarValorDeN(n, inicio) {
  console.log('* n:', n);
  console.log('* Transcurrido:', new Date() - inicio);
  console.log('***************************************');
}

async function main() {
  let n = 0;
  let inicio = new Date();

  setInterval(function () {
    n += 1;
    mostrarValorDeN(n, inicio);

    if(n%3== 0)  {
      request1.doRequest('https://umsa.bo', n);
    }
    if(n%5 == 0)  {
      request1.doRequest('https://www.twilio.com/blog/2017/08/http-requests-in-node-js.html', n);
    }
    if (n%7 == 0) {
      request1.doRequest('https://www.twilio.com/blog/2017/08/http-requests-in-node-js.html', n);
      request1.doRequest('https://umsa.bo', n);
    }
    
  }, 1000);

};

main();
