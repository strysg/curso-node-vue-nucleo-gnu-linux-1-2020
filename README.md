# Curso Node.js Vue.js Núcleo GNU/Linux I/2020

Sobre el curso: Este curso pretende ser un apoyo para quienes desarrollen en javascript y quieran repasar/aprender usando Node.js y Vue.js para desarrollo web.

> Los ejemplos e instalación de software de este curso se han probado en GNU/Linux Debian y ubuntu. Adicionalmente **no se hará uso de una cuenta de usuario con privilegios elevados** como root, durante el desarrollo de aplicaciones web no se debería usar una cuenta con privilegios elevados de sistema.

Contenido del curso versionado: https://gitlab.com/strysg/curso-node-vue-nucleo-gun-linux-1-2020 (se agradecen aportes ;D)

* Facilitador: Rodrigo Garcia Saenz <rgarcia@laotra.red>

* [Sesión 1](#sesi%C3%B3n-1) (Introducción a Node.js)
* [Sesión 2](#sesi%C3%B3n-2) (Gestor de paquetes NPM y ejercicios con Node.js)
* [Sesión 3](#sesión-3) (Gestión de BD relacional con ORM sequelize **require instalación de postgresql**)
* [Sesión 4](#sesi%C3%B3n-4) (Introducción a Vue.js)
* [Sesión 5](#sesi%C3%B3n-5) (Ejercicios Vue.js + compilación construcción con webpack básico)
* [Sesión 6](#sesi%C3%B3n-6) (Proyecto [Base-backend](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/) introducción y creación de nuevos módulos)
* [Sesión 7](#sesi%C3%B3n-7) (Proyecto [Base-backend](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/) capa de dominio y expocisión de nuevos endpoints)
* [Sesión 8](#sesi%C3%B3n-8) (Proyecto [Base-frontend](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-fronted/) creando la intefaz para el nuevo módulo)

## Sesión 1

### ¿Qué es node.js?

Node.js es un entorno de ejecución javascript manejado por eventos y asíncrono [[1]](https://nodejs.org/en/about/). Este entorno nos permite ejecutar código javascript fuera de un navegador web para por ejemplo escribir software del lado del servidor web apoyando el paradigma "*Javascript everywhere*". Node.js es un proyecto de código abierto y es ampliamente usado.

### Instalación de Node.js

Hay varias formas de instalar Node.js por ejemplo constuyendo desde el código fuente [ [2] ](https://github.com/nodejs/node/blob/master/BUILDING.md), pero por practicidad instalaremos node.js en un entorno local usando una versión ya construida.

Para no requerir permisos especiales ni hacer una instalación global a nivel de sistema usaremos el gesto NVM.

#### NVM

Es un manejador de versiones de Node.js que nos facilitará la instalación y manejo de distintas versiones de Node.js.

Siguiendo la guía oficial [[3]](https://github.com/nvm-sh/nvm#installing-and-updating), para instalar NVM se puede ejecutar:

```sh
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
```
Que descargará un script bash y lo ejecutará instalando nvm. Este script **no necesita ejecutarse con privilegios elevados**, se puede ejecutar como usuario sin privlegios. NVM mantiene las versiones de Node.js en el directorio local del usuario actual `~/.nvm/versions/node`.

Una vez se instale NVM y abirendo nuevamente la terminal, instalaremos la versioń 10 lts de node.js con:

```sh
nvm install 10
```
Esto descargará un binario de node.js, lo copiará en `~/.nvm/versions/node`.

Para probar que la instalación ha concluido correctamente ejecutamos en la terminal:

```sh
$ node --version
# que debería mostrar algo como
v10.16.1
# si solo ejecutamos se debería abrir la consola de node (REPL) y podríamos probar código javascript
$ node
```

### Event Loop

Un programa en node.js funciona manejando eventos, reacciona según estos ocurren. Este manejador de eventos se llama el *event loop* que ejeucta las operaciones siguendo un orden o fases.


![Event loop](https://miro.medium.com/max/1227/1*ROxiavz7LeRpIfcgRDE7CA.png)
Fuente: https://miro.medium.com/max/1227/1*ROxiavz7LeRpIfcgRDE7CA.png

* **timers**: Todo lo que es programado usando `setTimeout()` o `setInterval()`.
* **IO Callbacks**: La mayoría de los *callbacks* se procesan en esta fase. Todo el código escrito por usuarios Node.js esta en *callbacks* básicamente (por ejemplo un *callback* de una petición http entrante dispara una cascada de *callbacks*), este es el código del usuario.
* **IO Polling**: Se reúnen los nuevos eventos que se ejecutarán en el próximo ciclo.
* **Set Immediate**: Ejecuta todos los callbacks registrados via `setInmediate()`-
* **Close**: Se ejecutan todos los *callbacks* `on('close')`.

Todo lo que va en una aplicación Node.js se ejecuta a través del *event loop* [[4]](https://medium.com/the-node-js-collection/what-you-should-know-to-really-understand-the-node-js-event-loop-and-its-metrics-c4907b19da4c).

* Tick Frecuency: Número de *ticks* por unidad de tiempo.
* Tick Duration: El tiempo que dura un *tick*.

![Ejecutando tareas concurrentes](https://miro.medium.com/max/1024/0*WqAUxk2NCgJkaRvp.png)

La forma en la que actúa el event loop permite ejecutar tareas de forma **concurrente**.

Más información en [[4]](https://medium.com/the-node-js-collection/what-you-should-know-to-really-understand-the-node-js-event-loop-and-its-metrics-c4907b19da4c)


#### Hilos vs Procesos (thread vs process)

Los hilos y procesos son secuencias de ejecución independientes, sin embargo los hilos pretenecen a un proceso y se ejecutan en un espacio compartido de memoria y los procesos en espacios separados de memoria.

![Hilos](https://pediaa.com/wp-content/uploads/2018/07/Difference-Between-Process-and-Thread.png)

Fuente: https://pediaa.com/wp-content/uploads/2018/07/Difference-Between-Process-and-Thread.png

**Node.js ejecuta un solo hilo** y este es el hilo que ejecuta el event loop

### Ejemplo

Usaremos el [ejemplo 1](https://gitlab.com/strysg/curso-node-vue-nucleo-gun-linux-1-2020/-/tree/master/app1) en el cual se ejecutan tareas para probar la ejecución de tareas concurrentes. El siguiente gráfico lo resume.

Descargamos el proyecto con:

```sh
git clone https://gitlab.com/strysg/curso-node-vue-nucleo-gun-linux-1-2020.git
# o
wget https://gitlab.com/strysg/curso-node-vue-nucleo-gun-linux-1-2020/-/archive/master/curso-node-vue-nucleo-gun-linux-1-2020-master.zip
```
Lo ejecutamos con la terminal en la carpeta raíz del proyecto descargado:

```sh
node app1/main.js
```

![ejemplo event loop](https://gitlab.com/strysg/curso-node-vue-nucleo-gun-linux-1-2020/-/raw/master/doc/imgs/clase1.jpeg)

## Sesión 2

### Introducción

Hay un principio que se usa en la programación que dice: "Si quieres resolver un problema grande, divídelo en problemas pequeños". Así avanzas y vas resolviendo el problema mayor.

También uno de los principios de la filosofía UNIX[[5]](http://www.catb.org/~esr/writings/taoup/html/ch01s06.html#id2877537) dice:

> Make each program do one thing wel

Que un programa haga una cosa pero que la haga bien.

### Módulos y paquetes

Los **módulos** son partes de un programa que especifican de que otras partes depende y qué funcionalidades provee para que otros módulos los usen . La relación entre los módulos se llama dependecia. [[6]](https://eloquentjavascript.net/10_modules.html#modules_npm)

Un **paquete** es un pedazo de código que puede ser distribuido es decir copiado e instalado. Puede contener uno o muchos módulos y tiene información acerca de que otros paquetes este depende, usualmente viene con documentación explicando que hace así la gente que no quiere escribir esa funcionalidad todavía puede usarlo.

### NPM

NPM hace dos cosas

1. Es un servicio por internet donde se pueden descargar y subir paquetes (https://npmjs.com).
2. Permite manejar las dependecias, instalar/borrar y publicar paquetes mediante una interfaz de línea de comandos (*cli*)

Para la versión de node 10, npm viene integrado a node.

### Ejemplo

El ejemplo siguiente pretende cubrir los siguientes aspectos:

- Iniciar un proyecto con npm
- Instalar paquetes con npm
- Construir una app web simple con Node.js + HTML y javascript

Haremos una aplicación web para adivinar una palabra mediante un formulario web, al cabo de un número de intentos el juego terminará y se tendrá que intentar con otra palabra. Un diagrama representando los componentes es el siguiente:

```
 --------------------------                       --------------
| Página HTML + Javascript |    (1) Iniciar      | App Node.js  |
|                          | ---------------->   |  back-end    |
|                          |     datos juego     |              |
|                          |  <---------------   |              |
|                          |                     |              |
|                          |     (2) letra       |              |
|                          |  --------------->   |              |
|                          |      resultado      |              |
|                          |  <---------------   |              |
|                          |                     |              |
 --------------------------                       --------------
```

Tenemos una página HTML con javascript que hace peticiones AJAX a un back-end Node.js y recibe respuestas

1. Inicia un nuevo haciendo una petición GET al endpoint `/nuevo` del back-end y este le responde con datos para el juego en formato JSON.
2. Envía una letra escogida por el jugador y envía una petición POST al endpoint `/letra`, el back-end la valida y responde según es correcto o no. Una se terminan los intentos la respuesta tendrá datos para terminar el juego en victoria o derrota según corresponda.

Para este ejemplo usaremos:

- Node.js para procesar javascript
- [Express.js](https://expressjs.com/) como microframework web Node.js
- HTML, CSS y Javascript para la página estática.

#### paso 1 - Iniciar el proyecto e instalar dependencias.

Iniciaremos en una nueva carpeta por ejemplo `nodeTest1`, abrimos una terminal en esa carpeta y ejecutamos:

```
npm init
```

Se abrirá un diálogo para que llenemos datos para nuestro proyecto, estos pueden ir así para propósitos de prueba:

```
package name: (app2) 
version: (1.0.0) 
description: Aplicación Node.js de pruebas
entry point: (index.js) 
test command: 
git repository: 
keywords: testing
author: Rodrigo Garcia
license: (ISC) GPL-3.0-or-later
```

Una vez llenemos se nos preguntará si la informacioń es correcta y aceptaremos con `yes`. Una vez hecho esto se creará un archivo `package.json` con metadatos sobre nuestro proyecto, este archivo es editable y se irá modificando según instalemos paquetes con npm o neceistemos cambiar información.

Ahora instalamos **express.js** usando npm:

```
npm install express --save
# Que debería responder
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN app2@1.0.0 No repository field.

+ express@4.17.1
added 50 packages from 37 contributors and audited 126 packages in 15.539s
found 0 vulnerabilities
```

Esto descargará el paquete express en su última versión estable y lo hará disponible en nuestro proytecto, además han ocurrido tres cosas:

1. Se ha creado una carpeta `node_modules` que a partir de ahora contendrá **todos** los paquetes que usemos como dependencias para nuestro proyecto, si la revisamos encontraremos `express.js` y todas sus dependencias.
2. Se ha modificado el archivo `package.json` y se ha agregado el campo `dependencies` con `"express": "^4.17.1"`. Esto ha ocurrido por que hemos usado la opción `--save` junto con `npm install`, entonces esto indica que nuestro proyecto depende de express en la versión 4.17.1 o mayor.
3. Se ha creado un archivo `package-lock.json` que tiene mayor detalle de los paqutes instalados y sus versiones, este archivo se usa para congelar versiones de paquetes cuando se instala el proyecto.

Con eso ya tenemos las dependencias necesarias instaladas.

#### paso 2 - Escribir el *entry point* y la configuración inicial del proyecto

En nuestro `package.json` hay un campo `main` que tiene el valor `index.js`, este indica que el punto de entrada (*entry point*) de nuestra aplicación es el archivo `index.js`. El *entry point* debe ser el que contenga el código que inicia la aplicación y carga los demás módulos como no existe debemos crearlo.

En el archivo `index.js` crearemos una instancia de express para que funcione como servidor que atienda nuestras peticiones HTTP en `localhost:3000` como se indica en la guía oficial [[7]](https://expressjs.com/en/starter/hello-world.html).

```javscript
'use strict';

const express = require('express');
// aqui creamos la instancia
const app = express();
const port  = 3000;

// definimos los endpoints
app.get('/',
        (req, res) => {
          res.send('¡Hola mundo!');
        });

// poniendo la aplicación en modo escucha en el puerto 3000
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
```

Ahora para probar que funcione abriremos en un navegador http://localhost:3000 y deberíamos ver en la pantalla "¡Hola Mundo!". Con esto ya tenemos los medios para escribir endpoints y hacer que la aplicación responda a peticiones HTTP gracias a express.

#### paso 3 - Definir endpoints y archivo con las adivinanzas

Ahora definieremos los *endpoints* que son las direcciones HTTP a las que la página HTML debe hacer las peticiones y la forma en que debe hacerlas, al recibir estas peticiones el servidor responderá y el código javascript en la página HTML deberá interpretar la respuesta y mostrarlas en la pantalla del navegador.

Primero definiermos el archivo con datos de las adivinanzas, la idea es que las adivinanzas posibles estén guardadas en un archivo que la aplicación pueda leer entonces para poder usarlo crearemos un archivo `adivinanzas.js` con el contenido:

```javascript
const adivinanzas = [
  {
    palabra: "conciencia",
    fprevia: "c  c  e  a",
    maxIntentos: 10,
    codigo: 'CU&wx31w'
  },
  {
    palabra: "comunicación",
    fprevia: "  mu  c  i n",
    maxIntentos: 10,
    codigo: '996143912551'
  },
  {
    palabra: "características",
    fprevia: " a  c  r   i",
    maxIntentos: 10,
    codigo: 'n2f551sca'
  },
  {
    palabra: "pragmáticamente",
    fprevia: "p           nte",
    maxIntentos: 15,
    codigo: 'p-3C1lw8541'
  },
  {
    palabra: "código",
    fprevia: "  d  o",
    maxIntentos: 10,
    codigo: 'F85[-1cff'
  },
];

module.exports = adivinanzas;
```
Con eso podremos importar ese archivo y si es necesario definir nuevas adivinanzas.

Ahora vamos a hacer que la petición HTTP que el usuario envié para intentar una letra sea una petición de tipo POST con cuerpo en formato JSON, para que la aplicación web pueda entender este mensaje instalaremos el paquete [body-parser](https://github.com/expressjs/body-parser) que permitirá *parsear* peticiones HTTP cuyo cuerpo sea en formato JSON. También instalaremos [cors](https://github.com/expressjs/cors) para el intercambio de recursos de origen cruzado.

```bash
npm install --save body-parser cors
```

Ahora modificaremos el archivo `index.js` agregando las siguientes líneas antes de los endpoints:

```javascript
const cors = require('cors');
const bodyParser = require('body-parser');

// definiendo política CORS
app.use(cors({
  origin: '*',
  methods: 'GET,POST',
  preflightContinue: false,
  headers: 'Cache-Control, Pragma, Content-Type, Authorization, Content-Length, X-Requested-With',
  'Access-Control-Allow-Headers': 'Authorization, Content-Type'
}));

// Ajustando para que la aplicación express acepte y pueda analizar mensajes tipo JSON
app.use(bodyParser.json({limit: '10mb', extended: true, strict: false }));
```

Luego continuearemos modificando este archivo.

#### paso 4 - Funciones que implementen la lógica en el back-end

Vamos a crear el archivo `utils.js` que tendrá funciones utilitarias para procesar las peticiones para empezar una nuevamente y para procesar los intentos cuando el usuario envía una letra, el archivo quedará asi:

```javascript
'use strict';
const adivinanzas = require('./adivinanzas');

let puntuacionActual = {
  restantes: 0,
  codigo: '',
  maxIntentos: 0,
  palabra: '',
  palabraActual: ''
};

const nuevaAdivinanza = () => {
  let adivinanza = adivinanzas[parseInt(Math.random()*adivinanzas.length)];
  puntuacionActual.restantes = adivinanza.maxIntentos;
  puntuacionActual.maxIntentos = adivinanza.maxIntentos;
  puntuacionActual.codigo = adivinanza.codigo;
  puntuacionActual.palabra = adivinanza.palabra;
  puntuacionActual.palabraActual = adivinanza.fprevia;
  return {
    palabraActual: puntuacionActual.palabraActual,
    codigo: puntuacionActual.codigo,
    restantes: puntuacionActual.restantes
  };
};

// obtiene todos los indeces de `val' en `arr'
// Fuente: https://stackoverflow.com/questions/20798477/how-to-find-index-of-all-occurrences-of-element-in-array#20798567
function getAllIndexes(arr, val) {
    var indexes = [], i = -1;
    while ((i = arr.indexOf(val, i+1)) != -1){
        indexes.push(i);
    }
    return indexes;
}

// Agregando funcion para reemplazar una cadena en un indice dado, en javascript las cadenas son inmutables
// Fuente: https://stackoverflow.com/questions/1431094/how-do-i-replace-a-character-at-a-particular-index-in-javascript
String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}

const verificarLetra = (letra) => {
  let indices = getAllIndexes(puntuacionActual.palabra, letra);
  indices.forEach(i => {
    console.log('>', puntuacionActual.palabraActual.replaceAt(i, puntuacionActual.palabra[i]));
    puntuacionActual.palabraActual  = puntuacionActual.palabraActual.replaceAt(i, puntuacionActual.palabra[i]);
  });
  return indices.length;
};

const intento = (codigo, letra) => {
  if (puntuacionActual.restantes == 0) {
    return {
      error: 'No hay intentos restantes',
      puntuacionActual: {
        palabraActual: puntuacionActual.palabraActual,
        codigo: puntuacionActual.codigo,
        restantes: puntuacionActual.restantes
      }
    };
  }
  if (codigo !== puntuacionActual.codigo) {
    return {
      error: 'Inválido',
      puntuacionActual: {
        palabraActual: puntuacionActual.palabraActual,
        codigo: puntuacionActual.codigo,
        restantes: puntuacionActual.restantes
      }
    };
  }
  puntuacionActual.restantes = puntuacionActual.restantes - 1;
  verificarLetra(letra);
  return {
    puntuacionActual: {
      palabraActual: puntuacionActual.palabraActual,
      codigo: puntuacionActual.codigo,
      restantes: puntuacionActual.restantes
    }
  };
};

const getPuntuacionActual = () => {
  return puntuacionActual;
};

module.exports = {
  nuevaAdivinanza,
  intento,
  getPuntuacionActual
};
```
Las funciones o variables que se van a usar en otros módulos las pone en el `module.exports` que es lo que este módulo expone hacia el exterior.

Para terminar con el back-end modificaremos el archivo `index.js` en los endpoints `/nuevo` y `/letra`, al final el archivo `index.js` quedará así:

```javascript
'use strict';

const express = require('express');
// aqui creamos la instancia
const app = express();
const port  = 3000;

const cors = require('cors');
const bodyParser = require('body-parser');

const utils = require('./utils');

// definiendo política CORS
app.use(cors({
  origin: '*',
  methods: 'GET,POST',
  preflightContinue: false,
  headers: 'Cache-Control, Pragma, Content-Type, Authorization, Content-Length, X-Requested-With',
  'Access-Control-Allow-Headers': 'Authorization, Content-Type'
}));

// Ajustando para que la aplicación express acepte y pueda analizar mensajes tipo JSON
app.use(bodyParser.json({limit: '10mb', extended: true, strict: false }));

// definimos los endpoints
app.get('/', (req, res) => {
  res.send('¡Hola mundo!');
});

// endpoint inicio para empezar un nuevo intento
app.get('/nuevo', (req, res) => {
  // seleccionando palabra para adivinar
  return res.status(200).send(utils.nuevaAdivinanza());
});

// endpoint para verificar letra
app.post('/letra', (req, res) => {
  // obteniendo valores enviados en el cuerpo json de la peticion
  const { codigo, letra } = req.body;
  if (codigo.length == 0 || letra.length == 0) {
    return res.status(200).send({ error: 'Debe enviar una letra'});
  }
  let resultadoIntento = utils.intento(codigo, letra);
  console.log('----', resultadoIntento, '\n----');
  if (resultadoIntento.error) {
    return res.status(200).send(resultadoIntento);
  }
  if (resultadoIntento.puntuacionActual.palabraActual.indexOf(' ') == -1) {
    // si no hay  espacios en blanco significa que encontró todas las letras
    return res.status(200).send({
      finalizado: true,
      puntuacionActual: resultadoIntento.puntuacionActual
    });
  }
  // enviando respuesta con status code 200
  return res.status(200).send(resultadoIntento);
});

// poniendo la aplicación en modo escucha en el puerto 3000
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
```

Cada que hagamos un cambio y para **ejecutar la aplicación** se tiene iniciar desde el entrypoint con:

```bash
node index.js
```

Para probar la funcionalidad del backend haremos peticiones HTTP usando `curl`, primero para crear un nuevo intento.

```bash
curl http://localhost:3000/nuevo
# Y el backend responde
{"palabraActual":"p           nte","codigo":"p-3C1lw8541","restantes":15}
```

Y luego tomando ese `codigo` hacemos una petición POST con cuerpo JSON para intentar adivinar con una letra.

```bash
curl -X POST http://localhost:3000/letra -H "Content-Type: application/json" -d '{"codigo": "p-3C1lw8541","letra": "a"}'
# la respuesta
{"puntuacionActual":{"palabraActual":"p a      a  nte","codigo":"p-3C1lw8541","restantes":14}}
```

#### paso 5 - Página HTML

Vamos a escribir una página HTML con javascript para implementar la interfaz del juego, el archivo `ìndex.html` quedará asi:

```html
<html>
  <head>
    <title>Prueba app node.js + HTML + javascript - Sesión 2</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="lang" content="es"/>
  </head>
  <body onload="reiniciar()">
    <h1>Adivina la palabra</h1>
    <div>
      <button onclick="reiniciar()">Nuevo</button>
      Intentos restantes: <label id="restantes">0</label>
      <div>
        <table id="actual" style="border: 1px solid black"></table>
      </div>
      Letra: <input id="letra" type="text" maxlength="1" size="1">
      <button onclick="enviarLetra()">Enviar Letra</button>
      <label id="respuesta"></label>
    </div>

    <!-- javascript -->
    <script language="javascript">
     var actual = {};

     function reiniciar() {
       console.log('reiniciar');
       actual = {};
       // peticion ajax para cargar nuevo
       let request = new XMLHttpRequest();
       request.responseType = 'json';
       request.open('GET', 'http://localhost:3000/nuevo', true);
       
       request.onload = function() {
         actual = request.response;
         document.getElementById("respuesta").innerHTML = '';
         actualizarRestantes();
         actualizarTablaPalabra();
       }
       request.send();
     }

     function enviarLetra() {
       var letra = document.getElementById("letra").value;
       console.log('letra:', letra);

       // enviando peticion
       let request = new XMLHttpRequest();
       request.open('POST', 'http://localhost:3000/letra', true);
       request.setRequestHeader("Content-Type", "application/json");
       request.responseType = 'json';

       request.onload = function() {
         var domRespuesta = document.getElementById("respuesta");
         if (request.response.error) {
           domRespuesta.innerHTML = 'Error:' + request.response.error;
           domRespuesta.style = "color: #C45325";
         } else if (request.response.finalizado) {
           domRespuesta.innerHTML = 'Ganaste';
           domRespuesta.style = "color: #14A845";
         } else {
           domRespuesta.innerHTML = '';
         }
         // console.log(request.response);
         actual.palabraActual = request.response.puntuacionActual.palabraActual;
         actual.restantes = request.response.puntuacionActual.restantes;
         // console.log('actual', actual);
         actualizarRestantes();
         actualizarTablaPalabra();
       };

       request.send(JSON.stringify({ codigo: actual.codigo, letra: letra }));
     }

     function actualizarTablaPalabra() {
       var domActual = document.getElementById("actual");
       domActual.innerHTML = '';
       // para visualizar en una tabla de resultados se arma una tabla con los resputados
       var tbody = document.createElement('tbody');
       var tr = document.createElement("tr");
       console.log('palabra actual', actual.palabraActual);
       for (let i=0; i<actual.palabraActual.length; i++) {
         var td = document.createElement("td");
         td.style = "border: 1px solid black; min-width: 10px;";
         td.innerHTML = actual.palabraActual[i];
         tr.appendChild(td);
       }
       tbody.appendChild(tr);
       domActual. DappendChild(tbody);
     }

     function actualizarRestantes() {
       var domRestantes = document.getElementById('restantes');
       domRestantes.innerHTML = actual.restantes;
     }
    </script>
    <!-- --->

  </body>
</html>
```
Entonces si abrimos `index.html` con un navegador podemos probar la página.

> El proyecto completo se puede revisar en [app2/](app2/) del curso.

![caputra](app2/captura.png)

## Sesión 3

En esta sesión veremos el uso básico de [sequelize](https://sequelize.org/) un [orm](https://es.wikipedia.org/wiki/Mapeo_objeto-relacional) para node.js que permite conectarse una base de datos relacional en este caso [postgresql](https://www.postgresql.org/).

En la aplicación anterior cada que se reiniciaba el servidor node, ningún dato se guardaba y todo volvía a sus valores iniciales, para hacer que los datos persistan una forma ampliamente usada es usar gestores de bases de datos relacionales, en este caso usaremos postgres.

### Instalación de postgresql (solo si no lo tenemos instalado)

Para nuestras pruebas, necesitamos conectarnos a una instancia de postgresql. Si no la tenemos podemos instalarla en nuestro sistema, desde debian bastaría con:

> **AVISO**: Esto hará cambios globales a nivel de sistema, si usas un sistema diferente de debian y busca la forma correcta de instalar postgres para tu sistema.

```bash
sudo apt update
sudo apt install postgresql
# Esto descargara la versión estable de postgres que se encuentra en los repositorios del sistema.
```

Una vez se termine de instalar probablemente tengamos que iniciar el servicio manualmente:

```bash
sudo postgresql start
```

#### Creación de base de datos de prueba

Si es una instalación nueva probamos conectándonos a la consola de postgres `psql` que se debería abrir con:

```bash
psql --username={usuario} --host={host}
# donde:
# - usuario: es el nombre del usuario en postgres.
# - host: el host o dirección IP
```

Si no recuerdas el usuario hay dos formas de conectarse.

##### Forma 1

Si es una instalación nueva podemos intentar cambiar la contraseña por defecto, para eso necesitamos **privilegios elevados de sistema** con el comando **su** o **sudo su** intentamos iniciar sersión en la terminal como superusuario, una vez lo tengas puedes hacer:

```bash
su - postgres
# abrirá una línea de comandos como usuario postgres
# una vez como 'postgres' abriremos psql
psql
```

A partir de ahora estamos usando la consola de postgres `psql`.

```sql
\password
-- Se abrirá un diálogo para que establezcamos la nueva contraseña para el usuario postgres en postgresql
```

##### Forma 2

Si tienes `sudo` funcionando correctamente pudes intentar:

```
sudo -u postgres psql
# esto iniciará psql como usuario postgres
```

#### creación de la base de datos de prueba

Ahora crearemos una base de datos de pruebas en la consola `psql`:

```sql
CREATE DATABASE pruebas1;
-- verificamos que se haya creado
\l
-- aqui se listan las bases de datos y debería aparecer una línea para pruebas1, por ejemplo

                              List of databases
   Name    |  Owner   | Encoding | Collate |  Ctype  |   Access privileges
-----------+----------+----------+---------+---------+-----------------------
 postgres  | postgres | UTF8     | C.UTF-8 | C.UTF-8 |
 pruebas1  | postgres | UTF8     | C.UTF-8 | C.UTF-8 |
 template0 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
           |          |          |         |         | postgres=CTc/postgres
 template1 | postgres | UTF8     | C.UTF-8 | C.UTF-8 | =c/postgres          +
           |          |          |         |         | postgres=CTc/postgres
```

Ahora cerramos psql.

### Instalación de squelize

La ventaja de usar un orm como sequelize es que podemos conectarnos y trabajar con distintos gestores de bases de datos como Postgres, MySQL, MariaDB, sqlite y sql server [[8]](https://sequelize.org/), instalamos sequelize usando npm:

```bash
npm install --save sequelize
```

### Probando la conexión

Ahora que tenemos una base de datos para probar en postgres, usarmos sequelize para conectarnos, para probar la conexión crearemos una nueva carpeta: `test2` y ahí seguimos los pasos habituales para iniciar un proyecto npm:

```bash
cd test2
npm init
npm install --save sequelize pg
```

Ahora crearemos un nuevo archivo `main.js` con el siguiente contenido:

```javascript
const { Sequelize } = require('sequelize');

const configPostgres = {
  database: "pruebas1", // nombre de la BD
  dialect: "postgres",  // Dialecto postgres
  host: "localhost",    // host o direccion de la maquina
  password: "123456",   // password
  username: "postgres"  // nombre de usuario
};

async function main() {

  const sequelize = new Sequelize(configPostgres);
  // probando conexion
  try {
    await sequelize.authenticate(); 
    console.log("conectado satisfactoriamente");
  } catch(error) {
    console.error("Error al conectarse a la BD:", error);
  }
}

main();
```

Una vez lo guardamos y ajustamos segun los datos de conexión podemos probar con:

```
node main.js
```

### Creando tablas y modelando

Para crear tablas y relaciones desde node.js y aprovechando sequelize, modelaremos las tablas a partir de objetos javascript, **cada tabla** en la base de datos **será un modelo**, básicamente lo siguiente:

1. Crear una carpeta `models` donde guardaremos un archivo por modelo.
2. Editaremos cada modelo y definiremos los campos de la tabla a partir de objetos javascript.
3. Cargaremos todos los modelos apuntando a la carpeta `models`.

#### Esquema

Es útil como primer paso hacer un diagrama que nos ayude a "visualizar" el modelo de datos, el siguiente esquema muestra las tablas que vamos a crear en la base de datos y según esto vamos a crear los modelos.

![diagrama-bd](app3/diagrama-bd.jpeg)

#### Script de creación en postgresql

Por ahora usaremos un script para crear las tablas en la base de datos según el esquema de la imagen anterior, crearemos un archivo `creacion.sql` con las instrucciones sql para crear las relaciones.

```sql
create table lugares (
       id serial primary key not null,
       nombre varchar(100),
       descripcion varchar(5000),
       latitud decimal,
       longitud decimal
);

create table carreteras (
       id serial primary key not null,
       nombre varchar(200)
);

create table rutas (
       id serial primary key not null,
       salida varchar(20), -- para simplificar usaremos varchar en lugar de time
       llegada varchar(20), -- ...
       expedita boolean not null DEFAULT true,
       -- llaves foraneas
       id_origen int references lugares(id),
       id_destino int references lugares(id),
       id_carretera int references carreteras(id)
);

-- Valores iniciales de pruebas
insert into lugares (nombre, descripcion, latitud, longitud) values ('Villa Vilaque', 'Villa Vilaque frente', -16.4315, -68.3092);
insert into lugares (nombre, descripcion, latitud, longitud) values ('Santa Ana', '-', -16.4061, -68.3401);
insert into lugares (nombre, descripcion, latitud, longitud) values ('San Calixto', 'Área San Calixto', -16.2707, -68.4653);
insert into lugares (nombre, descripcion  , latitud, longitud) values ('Puerto Perez', 'Puerto Perez en Batallas', -16.3075, -68.5195);

insert into carreteras (nombre) values ('RN-2');
insert into carreteras (nombre) values ('Camino a Peñas');

insert into rutas (salida, llegada, expedita, id_origen, id_destino, id_carretera) values ('14/03/2020 14:04:29', '14/03/2020 14:35:11', true, 1, 2, 1);
insert into rutas (salida, llegada, expedita, id_origen, id_destino, id_carretera) values ('14/03/2020 14:36:09', '14/03/2020 14:45:11', true, 2, 3, 1);
insert into rutas (salida, llegada, expedita, id_origen, id_destino, id_carretera) values ('14/03/2020 14:50:09', '14/03/2020 15:20:01', true, 3, 4, 2);

insert into rutas (salida, llegada, expedita, id_origen, id_destino, id_carretera) values ('15/03/2020 19:12:05', '15/03/2020 19:45:11', true, 3, 4, 2);
insert into rutas (salida, llegada, expedita, id_origen, id_destino, id_carretera) values ('15/03/2020 20:30:31', '14/03/2020 20:45:46', true, 4, 3, 2);
```

Guardaremos el archivo y lo ejecutaremos en `psql`, para esto abrimos psql de cualquiera de las formas que se mencionan arriba y dentro de la consola de postgres seguimos:

```sql
-- la siguiente instruccion hace que nos conectemos a la base de datos pruebas 1
\c pruebas1

-- una vez nos conectamos, vamos a ejecutar el script creacion.sql (asumimos que se abrio psql en la misma ruta
-- del archivo creacion.sql)
\i creacion.sql

-- para verificar la creacion 
\dt
-- que devuelve
           List of relations
 Schema |    Name    | Type  |  Owner   
--------+------------+-------+----------
 public | carreteras | table | postgres
 public | lugares    | table | postgres
 public | rutas      | table | postgres
(3 rows)

select * from lugares;
```

Con esas instrucciones ya tenemos creadas las tablas y hemos introducido algunos datos de pruebas.

### Creando modelos con sequelize

Un modelo es una abstracción que representa una tabla en la base de datos. En Sequelize, es una clase que extiende `Model`. El modelo le dice a sequelize algunas cosas sobre la entidad que representa, como el nombre de la tabla en la base de datos y qué columnas (y el tipo de datos) tienen [[9]](https://sequelize.org/master/manual/model-basics.html).

Como se mencionó anteriormente crearemos una carpeta llamada `models` donde los guardaremos. Lo que queremos es lograr una arboresencia así en nuestro proyecto.

```
├── creacion.sql
├── db.js
├── associations.js
├── main.js
├── package.json
├── package-lock.json
└── models
    ├── carreteras.js
    ├── lugares.js
    └── rutas.js
```

Ahora crearemos el archivo `models/lugares.js` con el contenido:

```javascript
'use strict';

const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  // definimos las columnas de la tabla lugares
  let fields = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER,
      xlabel: 'ID'
    },
    nombre: {
      type: DataTypes.STRING(100),
      defaultValue: 'Sin nombre',
      allowNull: false
    },
    descripcion: {
      type: DataTypes.STRING(5000),
      defaultValue: 'Sin descripción',
      allowNull: false
    },
    latitud: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    longitud: {
      type: DataTypes.DECIMAL,
      allowNull: false
    }
  };

  let Lugares = sequelize.define('lugares', fields, {
    timestamps: false,
    tableName: 'lugares'
  });

  return Lugares;
};
```
`models/carreteras.js`:
```javascript
'use strict';

const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER,
      xlabel: 'ID'
    },
    nombre: {
      type: DataTypes.STRING(200),
      defaultValue: 'No nombrada',
      allowNull: false
    }
  };

  let Carreteras = sequelize.define('carreteras', fields, {
    timestamps: false,
    tableName: 'carreteras'
  });

  return Carreteras;
};

```
`models/rutas.js`:
```javascript
'use strict';

const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER,
      xlabel: 'ID'
    },
    salida: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: '01/01/1980 00:00:00', // Formato DD/MM/AAAA HH:MM:SS      
    },
    llegada: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: '01/01/1980 00:00:00', // Formato DD/MM/AAAA HH:MM:SS      
    },
    expedita: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false
    }
  };

  let Rutas = sequelize.define('rutas', fields, {
    timestamps: false,
    tableName: 'rutas'
  });

  return Rutas;
};
```
Si se necesitaría más tipos de datos podemos consutlar la [documentación oficial de Sequelize sobre tipos de datos](https://sequelize.org/master/variable/index.html#static-variable-DataTypes).

### Creando Asociaciones

Ya tenemos definidos los modelos en archivos separados siguiendo la misma estrucutra y campos que el archivo `creacion.sql`, pero habrás notado que en el archivo `models/rutas.js` no están definidas las relaciones de dependencia `id_origen id_destino id_carretera`. Con Sequelize se puede definir estas dependencias mediante su función [associate()](https://sequelize.org/master/identifiers.html#associations) en un archivo separado, en este caso usaremos `associations.js` donde especificaremos estas relaciones y su contenido es:

```javascript
'use strict';

module.exports = function associations (models) {
  const { lugares, carreteras, rutas } = models;

  // Representa: id_origen int references lugares(id),
  rutas.belongsTo(lugares, { foreignKey: { name: 'id_origen', allowNull: false }, as: 'lugar_origen' });
  lugares.hasMany(rutas, { foreignKey: { name: 'id_origen', allowNull: true }, as: 'lugar_origen' });

  // Representa: id_destino int references lugares(id),
  rutas.belongsTo(lugares, { foreignKey: { name: 'id_destino', allowNull: false }, as: 'lugar_destino' });
  lugares.hasMany(rutas, { foreignKey: { name: 'id_destino', allowNull: true }, as: 'lugar_destino' });

  // Representa: id_carretera int references carreteras(id)
  rutas.belongsTo(carreteras, { foreignKey: { name: 'id_carretera', allowNull: false }, as: 'carretera' });
  carreteras.hasMany(rutas, { foreignKey: { name: 'id_carretera', allowNull: true }, as: 'carretera' });

  return models;
};
```
### Cargando modelos

Una ve los tenemos escritos tenemos que cargarlos junto con nuestra conexión a la base de datos, quizá notaste que los archivos en `models/` y el mismo `associations.js` no se "importan" los módulos usando `require`, en su lugar **reciben** los parámetros (al definir `module.exports`). Esto quiere decir que desde otro "lugar" al cargar estos archivos se les está **pasando** estos parámetros, entonces necesitamos escribir desde donde se los carga. Para ello definiremos el archivo `db.js`:

```javascript
'use strict';

const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');

// algunos utilitarios
function removeAll (elements, list) {
  var ind;

  for (var i = 0, l = elements.length; i < l; i++) {
    while ((ind = list.indexOf(elements[i])) > -1) {
      list.splice(ind, 1);
    }
  }
}

/**
 * Carga los modelos de la carpeta especificada
 *
 * @param {string} PATH: Path del directorio de donde se cargará los modelos del sistema
 * @param {object} sequelize: Objeto Sequelize
 */
function loadModels (PATH, sequelize) {
  let files = fs.readdirSync(PATH);
  let models = {};

  // por cada archivo obtenido se carga un modelo (volviendo a llamar a esta misma funcion)
  files.forEach(function (file) {
    let pathFile = path.join(PATH, file);
    if (fs.statSync(pathFile).isDirectory()) {
      models[file] = loadModels(pathFile, sequelize);
    } else {
      file = file.replace('.js', '');
      models[file] = sequelize.import(pathFile);
    }
  });

  return models;
}

// campos extra para guardar la fecha de creacion/modificacion (opcional)
const timestamps = {
  _created_at: {
    type: Sequelize.DATE,
    allowNull: false,
    label: '_created_at',
    defaultValue: Sequelize.NOW
  },
  _updated_at: {
    type: Sequelize.DATE,
    xlabel: 'fields._updated_at'
  }
};

function setTimestamps (fields) {
  return Object.assign(fields, timestamps);
}

module.exports = {
  loadModels,
  setTimestamps
};
```

Lo que nos quedá es cargar `db.js` y `associations.js` lo haremos desde el *entry point* que es `main.js`:

```javascript
'use strict';

const { Sequelize } = require('sequelize');
const path = require('path');

const db = require('./db');
const associations = require('./associations');

const configPostgres = {
  database: "pruebas1", // nombre de la BD
  dialect: "postgres",  // Dialecto postgres
  host: "localhost",    // host o direccion de la maquina
  password: "developer1",   // password
  username: "developer"  // nombre de usuario
};

async function main() {

  const sequelize = new Sequelize(configPostgres);
  // probando conexion
  try {
    await sequelize.authenticate(); 
    console.log("✓ Conectado satisfactoriamente");
  } catch(error) {
    console.error("Error al conectarse a la BD:", error);
  }

  // cargando modelos
  let _models;
  try {
    _models = db.loadModels(path.join(__dirname, 'models'), sequelize);
    console.log('✓ Modelos cargados exitosamente.');
  } catch (error) {
    console.error('✕ Error cargando modelos', error);
    process.exit(1); // finalizando con codigo de error
  }
  // cargando asociaciones
  let models;
  try {
    models = associations(_models);
    console.log('✓ Asociaciones cargadas exitosamente.');
  } catch (error) {
    console.error('✕ Error cargando asociaciones', error);
    process.exit(1); // finalizando con codigo de error
  }

  console.log(' *** Modelos cargados ***');
  console.log(models);
}

main();
```
Ahora probaremos el cargado ejecutando en la carpeta principal del proyecto:

```bash
node main.js
```

### Consultando y escribiendo registros

Si todo fue bien ahora modificaremos un poco el archivo `main.js`, le agregaremos **express.js** para poder atender unas cuantas peticiones HTTP, definiermos endpoints para consultas a la base de datos y modificaciones de prueba.

> Puedes encontrar todo este proceso en [app3/](app3/)

Primero algunos paquetes para facilitar la atención de peticiones HTTP.

```bash
npm install --save express body-parser cors express-asyncify
```

Luego vamos a modificar el archivo `main.js` para agregarle express y algunos endpoints para:

- Listar rutas y lugares de la BD.
- Introducir nuevos lugares en la BD.
- Borrar un lugar de la BD.

Aprovecharemos para ordenar un poco y usar `try` y `catch` para gestionar excepciones, así en caso de un procedimiento anormal lo que nos gustaría hacer es deterner lo que se estaba haciendo inmediatamente y saltar al lugar que sabe como manejar ese problema, eso es lo que hace el manejo de excepciones [[10]](https://eloquentjavascript.net/08_error.html#h_zT3755/aOp)

```javascript
'use strict';

const { Sequelize } = require('sequelize');
const path = require('path');
const express = require('express');
const asyncify = require('express-asyncify');
const port = 3000;

const cors = require('cors');
const bodyParser = require('body-parser');

const db = require('./db');
const associations = require('./associations');

const configPostgres = {
  database: "pruebas1", // nombre de la BD
  dialect: "postgres",  // Dialecto postgres
  host: "localhost",    // host o direccion de la maquina
  password: "123456",   // password
  username: "postgres"  // nombre de usuario
};

const sequelize = new Sequelize(configPostgres);
const router = asyncify(express.Router());
const app = express();

function inicializarExpress() {
  try {
    // definiendo política CORS
    app.use(cors({
      origin: '*',
      methods: 'GET,POST',
      preflightContinue: false,
      headers: 'Cache-Control, Pragma, Content-Type, Authorization, Content-Length, X-Requested-With',
      'Access-Control-Allow-Headers': 'Authorization, Content-Type'
    }));
    // ajustando para poder recibir JSON
    app.use(bodyParser.json({limit: '10mb', extended: true, strict: false }));

    // para usar router en lugar de app.use
    router.use((req, res, next) => {
      Object.setPrototypeOf(req, app.request);
      Object.setPrototypeOf(res, app.response);
      req.res = res;
      res.req = req;
      next();
    });
    app.use('/', router);
  } catch (error) {
    console.error('Hubo un error al inicializar express,', error);
    throw new Error("Error inicializando express:" + error);
  }
}

async function inicializarBD() {
  // probando conexion
  try {
    await sequelize.authenticate(); 
    console.log("✓ Conectado satisfactoriamente");
  } catch(error) {
    console.error("Error al conectarse a la BD:", error);
  }
  // cargando modelos
  let _models;
  try {
    _models = db.loadModels(path.join(__dirname, 'models'), sequelize);
    console.log('✓ Modelos cargados exitosamente.');
  } catch (error) {
    console.error('✕ Error cargando modelos', error);
    throw new Error("Error cargando modelos en la BD:", error);
  }
  // cargando asociaciones
  let models;
  try {
    models = associations(_models);
    console.log('✓ Asociaciones cargadas exitosamente.');
  } catch (error) {
    console.error('✕ Error cargando asociaciones', error);
    throw new Error("Error cargando asociaciones en la BD:", error);
  }

  console.log(' *** Modelos cargados ***');
  console.log(models);
  return models;
}

async function definirEndpoints(models) {
  router.get('/', (req, res) => {
    res.status(200).send("Hola");
  });

  // endpoint para listar lugares
  router.get('/lugares', async (req, res) => {
    const { lugares } = models;
    let respuesta = await lugares.findAndCountAll({});
    res.status(200).send({ lugares: respuesta });
  });

  // endpoint introducir lugares
  router.post('/add-lugar', async (req, res) => {
    const { lugares } = models;
    const { lugar } = req.body;
    console.log('Recibido:', lugar);
    let resultado = await lugares.create(lugar);
    res.status(200).send(resultado);
  });

  router.get('/lugares-nombre/:nombre', async (req, res) => {
    // tarea: Retornar solo los lugares que tengan el :nombre
    const { nombre } = req.params;
    // ... consutlar aqui
    // res.status(200).send({ lugares: ... })
  });
}

async function main() {
  try {
    inicializarExpress();
  } catch (error) {
    console.error('Error iniciando express:', error);
    process.exit(1);
  }

  let models;
  try {
    models = await inicializarBD();
  } catch (error) {
    console.error('Error iniciando BD:', error);
    process.exit(1);
  }

  try {
    definirEndpoints(models);
  } catch (error) {
    console.error('Error al definir endpoints:', error);
    process.exit(1);
  }

  app.listen(port, () => console.log(`Aplicación escuchando en puerto: ${port}!`));
}

main();
```

Ahora para probar iniciaremos la aplicación con `node main.js` y probaremos por ejemplo el introducir un nuevo lugar desde otra consola usando curl:

```bash
curl http://localhost:3000/add-lugar -X POST -H "Content-Type: application/json" -d '{ "lugar": { "nombre": "ln1", "descripcion": "Nuevo lugar", "latitud": -16.231, "longitud": "-68.3400" }}'
```

## Sesión 4

[Vue.js](https://vuejs.org/) es un framework javascript que nos facilitará el desarrollo de interfaces de usuario de manera ordenada y escalable. El desarrollo de palataformas web puede hacerse sin usar frameworks usando código javascript nativo, pero podemos aprovechar las funcionalidades y características de un framework que implementa ciertos patrones de diseño[[11]](https://es.wikipedia.org/wiki/Patr%C3%B3n_de_dise%C3%B1o) y nos facilita y agiliza la solución de problemas **que alguien más ya ha solucionado** y los ha identificado como un patrón comúm.

Otra ventaja de usar un framework como vue.js es que **las funcionalidades ya se han probado** y al ser este de código abierto han sido revisadas, probadas y mejoradas por muchas personas.

Para la continuación de este curso vamos a hacer un corto ejemplo del tutorial de vue.js.

### Primeras pruebas

Vamos a saltarnos el paso de instalación con npm descrito en [[12]](https://vuejs.org/v2/guide/installation.html) por ahora para simplificar las pruebas, en otra sesión veremos la instalación usando npm, web-pack, vue-cli y otros para compilar y construir `tempaltes` vue.

Ahora creamos un archivo `index1.html`:

```html
<html>
  <head>
    <title>Prueba app Vue.js - Sesión 4</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Incluimos la version de desarrollo (require conexión a internet) -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  </head>
  <body>
    <div id="app">
      {{ message }}
    </div>

    <div id="app-2">
      <button v-bind:title="message">
        Pon el mouse sobre mi algunos segundos para ver
        mi título vinculado dinámicamente.
      </button>
    </div>
  </body>

  <!-- Codigo javascript para esta pagina -->
  <script>
   var app = new Vue({
     el: '#app',
     data: {
       message: 'Hola Vue!'
       
     }
   });

   var app2 = new Vue({
     el: '#app-2',
     data: {
       message: 'Cargaste esta página en ' + new Date().toLocaleString()
     }
   })
  </script>
</html>
```
Abrimos el archivo `ìndex1.html` en el navegador y deberíamos poder probar.

Todos los valores dentro de la propiedad `data` de una instancia de vue, **reaccionan reactivamente** es decir que cuando el valor de la variable cambia, se actualiza automáticamente el elemento *DOM* donde la estemos usando [ [13] ](https://vuejs.org/v2/guide/instance.html).

Cada instancia de vue pasa por una serie de pasos al ser creada y durante estos pasos se puede hacer que se ejecuten instrucciones dentro de propiedades especiales llamadas **lifecycle hooks** y estos son: *beforeCreate, created, beforeMount, mounted, beforeUpdate, updated, beforeDestroy, destroyed*.

![Vue's Lifecycle diagram](https://vuejs.org/images/lifecycle.png?_sw-precache=6f2c97f045ba988851b02056c01c8d62)
Fuente: https://vuejs.org/images/lifecycle.png?_sw-precache=6f2c97f045ba988851b02056c01c8d62

### Explorando la sintaxis

Vamos a practicar con la sintaxis de Vue.js para actualizar los valores *[DOM](https://es.wikipedia.org/wiki/Document_Object_Model)* de una página pequeña, creamos un nuevo archivo llamado `ìndex2.html`.

```html
<html>
  <head>
    <title>Prueba app2 Vue.js - Sesión 4</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <div id="vmEjemplo">
      <!-- la notacion {{ variable }} sirve para modificar el texto html solamente -->
      {{ mensaje }}
      <!-- asociaremos v-bind:style con la variable estilo -->
      <p v-bind:style="estilo" >
        <!-- Esta notación reemplaza *texto HTML* solamente -->
        Ancho actual (px): {{ ancho }}
      </p>
      <input v-model="ancho">
    </div>
  </body>

  <!-- Codigo javascript para esta pagina -->
  <!-- Incluimos la version de desarrollo -->
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script>
   var vmEjemplo = new Vue({
     el: '#vmEjemplo',
     data: {
       // valores iniciales
       estilo: {
         backgroundColor: 'gray',
         width: '200px',
       },
       ancho: 'Ancho por defecto 200px',
       mensaje: 'Introduzca número'
     },
     // propiedad watch, reacciona cuando hay cambios
     watch: {
       // se dispara al encontrar cambios en 'ancho'
       ancho: function(valor) {
         this.cambiarEstilo(valor);
       }
     },
     methods: {
       cambiarEstilo: function (ancho) {
         // controlando si es un numero
         if (isNaN(ancho)) {
           this.mensaje = 'Debe introducir un número';
         } else {
           this.estilo.width = ancho + 'px';
           this.mensaje = 'Ancho actual:' + ancho + 'px';
         }
         console.log('this.estilo.width', this.estilo.width);
       }
     }
   });
  </script>
</html>
```

El código de arriba también se ejecuta abriendo en el navegador `index2.html`, en el que se usan la notación `{{ }}` y también `v-bind:style`, este último **asocia** la propiedad `style` del elemento con el variable `estilo` declarada mas abajo. El ejemplo básicamente cambia el ancho de un párrafo modificando la propiedad `style` según se cambia el texto la caja de texto de abajo, también controlando si el texto introducido es un número.

La caja de texto usa `v-model` para asociar el valor de la caja de texto con la variable `ancho`.

![sel1](/app4/sel1.png)
![sel2](/app4/sel2.png)

### Ejercicio

1. Usando el ejemplo anterior, modifique el archivo `index2.html` para agregarle un botón que al hacer click cambie el fondo del párrafo a un color aleatorio, (pista: https://www.w3resource.com/javascript-exercises/javascript-math-exercise-40.php).

2. Agregue un *input* y una tabla HTML, al escribir en el input por ejemplo 2, se crearán dos filas en la tabla con texto ascendente, por ejemplo:

Si el input es 4:

| No. |
|----|
| 1  |
| 2  |
| 3  |
| 4  |

Si el input es n:

| No. |
|----|
| 1  |
| 2  |
|... |
| n  |

Las soluciones las puedes ir pegando en [https://pad.nucleognulinux.org/zsN5YX1EQve0ibJ9LVaO2g?both](https://pad.nucleognulinux.org/zsN5YX1EQve0ibJ9LVaO2g?both).

## Sesión 5

Vue.js no impone una estructura rígida para nuestro proyecto y nos permite que la vayamos definiendo según necesidad. En esta podemos definir componentes vue, los componentes vue son instancias vue reutilizables en otras instancias. [ [14 ] ](https://vuejs.org/v2/guide/components.html). Podemos aprovechar estos componentes para **importar** un componente dentro de otro y aprovechar las funcionalidades que ya tiene. El proceso de definir la estructura del proyecto Vue es algo que no cubriremos en este curso y solo utilizaremos la de proyectos donde esta estructura y organización de los componentes vue ya están definidas.

Esto nos permitirá centrarnos más en el desarrollo en proyectos donde ya se ha establecido una estructura y se ha ajustado un sistema de construcción como [webpack](https://webpack.js.org/guides/getting-started/) para "empaquetar" el código y recursos de nuestro proyecto en un único recurso javascript que nos permitirá trabajar en un entrono de desarrollo y **construir** una versión de nuestro proyecto optimizada para producción.

### Ejemplo plantilla vue 2 boilerplate

Vamos a usar [https://github.com/petervmeijgaard/vue-2-boilerplate](https://github.com/petervmeijgaard/vue-2-boilerplate) como proyecto de ejemplo para desarrollar aplicaciones *SPA* (Single page aplications) en esta sesión.

#### Paso 1. Descargar

En una carpeta descargamos el proyecto.

```
git clone https://github.com/petervmeijgaard/vue-2-boilerplate
# o descargar como zip
```

#### Paso 2. Instalar dependencias

Ingresamos a la carpeta del proyecto recién descargado:

```
cd vue-2-boilerplate/
```

Este proyecto ya tiene definidas las dependencias en un archivo `package.json` y también scripts para ejecutarlo, para instalar las dependencias nos bastará con:

```
npm install
```

Descargará todas las dependencias y las guardará en la carpeta `node_modules` como habíamos hecho en ejemplos anteriores, una vez termine si revisamos el archivo `package.json` hay un campo llamado `scripts`:

```javascript
  "scripts": {
    "serve": "vue-cli-service serve",
    "build": "vue-cli-service build",
    "test": "vue-cli-service test",
    "e2e": "vue-cli-service e2e",
    "lint": "vue-cli-service lint"
  },
```
Estos son órdenes personalizadas para que podamos ejecutarlas con npm, por ejemplo al usar `npm run build` se ejecutará `vue-cli-service build`. Una de estas órdenes es `npm run serve` que lo que hace es iniciar un servidor en modo desarrollo con nuestra aplicación, entonces ejecutaremos:

```
npm run serve
```

Y si abrimos un navegador en http://localhost:8080 podremos usar la aplicación en modo desarrollo y también se recarga automáticamente cuando detecta un cambio en un archivo de código. En el navegador se mostrará una pantalla de *login* donde podemos registrarnos y crear una cuenta de pruebas.

#### Paso 3. Revisar

Ahora que funciona podemos revisar como esta estructurado el proyecto, tratar de entenderlo y hacer modificaciones para ver que provoca. 

Afortunadamente el proyecto que descargamos tiene documentación que describe los [archivos principales y la estructura de directorios](https://github.com/petervmeijgaard/vue-2-boilerplate#directory-structure), de aquí podemos ver algunos archivos y características interesantes:

- El archivo principal o *entry point* de la aplicación `src/App.vue` en la propiedad `mounted()` hay una línea `if (this.$store.state.auth.authenticated)` que consulta sobre la propiedad `auth.authenticated` del **almacén** vuex, este es un sistema de memoria que permite almacenar valores que se pueden compartir en todos los componentes vue y del proyecto.
- El archivo `src/main.js` es el encargado de inicializar la aplicación, en este se **importan** otros módulos javascript y componentes vue, también el almacén vuex que se define como `store`.
- Existe un archivo `router/index.js` donde se definen las rutas y redirecciones usando el sistema de rutas [vue-router](https://router.vuejs.org/).
- Hay muchos directorios con archivos `.vue` como `components/ views/` donde se definen componentes vue y muestran ejemplos de como estos se pueden incluir en otros, la idea general de un componente es que haga una tarea y que si hay la necesidad se pueda usar desde otro lugar.
- El directorio `layout/` es para colocar archivos de *templates* vue que se pueden incluir como componentes, hay varias formas de hacerlo en general hay ejemplos en [[15]](https://vuejsdevelopers.com/2017/03/24/vue-js-component-templates/).
- Hay un directorio `plugins` para poder usar librerías adicionales.
- Hay un directorio `mixins/` que incluyen [mixins](https://vuejs.org/v2/guide/mixins.html) que son una forma de extender funcionalidades para componentes vue.

Hay seguramente muchas otras características y deberíamos consultar con la documentación de ese proyecto vue-2-boilerplate en particular que esta diseñado para practicar y aprender vue.js.

#### Paso 4. Modificar para probar funcionalidades

Viendo un poco como funciona este proyecto ahora nos daremos la tarea de agregar un nuevo componente y crear una nueva ruta. Instintivamente seguramente vamos a imitar y copiaremos tal y como se hace con otros en el proyecto como Card.vue o Register.vue.

Primero vamos a crear un nuevo componente muy pequeño cuya función será mostrar un *post* o mensaje según se escriba. Nuestro componente **aceptará** dos propiedades que serán un **tema** y un **mensaje**, definimos el nuevo componente en el archivo `src/components/Post.vue`:

```html
<template>
  <!-- copiando de Card.vue -->
  <div class="card">
    <h4>
      {{ tema }}
    </h4>
    <p>
      {{ mensaje }}
    </p>
    <hr>
  </div>
</template>

<script>
export default {
  name: 'Post',
  props: {
    tema: {
      type: String,
      default: '',
    },
    mensaje: {
      type: String,
      default: '',
    },
  },
  data() {
    return {};
  },
};
</script>
```

Ahora creamos el archivo `src/views/Posts/Index.vue` que será la nueva **vista** de los *posts*, es decir la página que mostrará los *posts*:

```html
<template>
  <v-layout>
    <v-card contextual-style="dark">
      <span slot="header">
        Posts
      </span>
      <div slot="body">
        <form @submit.prevent="crearPost(nuevoPost)">
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fa fa-edit fa-fw"/>
                </span>
              </div>
              <input
                v-model="nuevoPost.tema"
                type="text"
                placeholder="Nombre del tema"
                class="form-control"
              >
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fa fa-edit fa-fw"/>
                </span>
              </div>
              <input
                v-model="nuevoPost.mensaje"
                type="text"
                placeholder="Nuevo mensaje"
                class="form-control"
              >
            </div>
          </div>
          <div class="form-group">
            <button class="btn btn-outline-primary">
              Publicar Post
            </button>
          </div>
        </form>
      </div>
      <div slot="footer">
        <h5>publicados</h5>
        <div id="posts">
          <!-- reutilizando componente Post de Post.vue -->
          <v-post
            v-for="post in posts"
            :key="post.id"
            :tema="post.tema"
            :mensaje="post.mensaje"
          />
        </div>
      </div>
    </v-card>
  </v-layout>
</template>

<script>
import VLayout from '@/layouts/Default.vue';
import VPost from '@/components/Post.vue';
import VCard from '@/components/Card.vue';

export default {
  name: 'Posts',
  components: {
    VLayout,
    VPost,
    VCard,
  },

  data() {
    return {
      nuevoPost: {
        tema: '',
        mensaje: '',
      },
      posts: [],
    };
  },

  methods: {
    crearPost(nuevoPost) {
      const npost = {
        id: this.posts.length + 1,
        tema: nuevoPost.tema,
        mensaje: nuevoPost.mensaje,
      };
      this.posts.push(npost);
    },
  },
};
</script>
```
En este archivo incluímos el componente `Post.vue` y agregamos un formulario con dos campos para crear nuevos posts; nombre del tema y mensaje. Cuando hagamos click en el botón "Publicar Post" se agregará una nueva instancia del componente `Post.vue` (que estamos importando como VPost) y se le pasará a esa instancia las propiedades definidas usando `v-for` que ayuda a recorrer y mostrar el array de posts que tengamos.

Luego se define una nueva **ruta** agregando en el archivo `src/routes/index.js` que hará que cuando se consulte a la ruta `posts/` se use el componente definido en `views/Posts/Index.vue`:

```javascript
  // Posts
  {
    path: '/posts',
    name: 'posts.index',
    component: () => import('@/views/Posts/Index.vue'),
    meta: {
      auth: true,
      guest: false
    },
  },
```

La propiedad `meta.auth` indica que se require autenticación ya que este proyecto puede verificar eso si lo necesitamos.

Finalmente agregamos una referencia a nuestra nueva ruta, aprovechando el archivo `src/layouts/Default.vue` agregamos:

```html
          <!-- para ver posts -->
          <router-link
            :to="{ name: 'posts.index' }"
            active-class="active"
            class="nav-item"
            tag="li"
          >
            <a class="nav-link">
              Posts
            </a>
          </router-link>
```

Que es un enlace a `posts.index` definido en `src/routes/index.js`. Al reiniciar la aplicación deberíamos poder ver la nueva ruta con todos los cambios que hicimos:

![app5/imgs/cap1.png](app5/imgs/cap1.png)

En conclusión **imitando** la forma de crear componentes de este proyecto hemos conseguido agregar una nueva página que muestra *posts* según vamos agregando llenando un formulario y hemos reutilizado otros componentes y *layouts* aprovechando las funcionalidades que ya tienen.

> Todas las modificaciones se pueden revisar en la carpeta app5/ de este curso.

Notarás que al reiniciar la aplicación todos los *posts* se pierden esto por que no estamos almacenando los posts para que perduren y esta tarea se hace normalmente en un servidor web o en el alacenamiento local del navegador por ejemplo, pero para fines de pruebas tenemos suficiente con eso.

En la siguiente sesión usaremos un proyecto final de backend con node.js y frontend con vue.js para realizar plataformas web, estos los proyectos https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base

## Sesión 6

En esta sesión usaremos el proyecto [base-backend](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base) publicado en el repositorio estatal de software libre de Bolivia, le agregaremos una tabla para registrar publicaciones y que estas sean revisadas por un usuario administrador y luego aprobadas o rechazadas, algo asi como un sistema de publicaciones (*posts*) que entran en moderación antes de ser publicados.

> [Video demo instalación](http://misc_publico.rmgss.net/varios/curso-node.js-vue.js-nucleognulinux-2020/bases-instalacion.mp4)

### Brevemente sobre el proyecto base-backend

Este proyecto lo escribió Omar Gutierrez desarrollador web y ex trabajador de la "AGETIC" en Bolivia, este proyecto se usa para crear poryectos con Node.js con lógica de aplicación típica para proyectos requeridos por instituciones estatales u otros de uso general.

Contiene caracterísitcas como arquitectura y estructura de código bien definida, funcionalidades gestión de usuarios, permisos, registro de entidades, módulos, integración con [firma digital Boliviana](http://firmadigital.bo/), integración con [ciudadanía digital Boliviana](https://www.gob.bo/ciudadania). A nivel de programación tiene utilidades como uso de ORM para la base de datos con seeders, migrations, pruebas unitarias, módulos de interoperabilidad, API rest y graphql, guardado de logs entre otros. 

El proyecto [base-frontend](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-frontend) esta bien integrado con este base-backend y facilita el desarrollo de plataformas web usando node.js y vue.js.

### Algunas notas sobre la instalación

Este proyecto requiere acceder a una base de datos postgresql por lo que necesitamos tener instalado postgresql y crear una base de datos de pruebas como indica el archivo INSTALL.md del proyecto base-backend. En la [sesión 3](#sesión-3) hemos instalado postgresql y mostrado como crear una base de datos. Luego necesitamos node.js, para este proyecto funciona con la versión 10.

Luego de crear los archivos `db.js mail.js y openid.js` como indica [el manual de instalación](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/-/blob/master/INSTALL.md#configuraciones), solo necesitaremos hacer cambios en el `db.js` poniendo las credenciales para conectarse a nuestra base de datos de pruebas.

Finalmente en caso de que al ejecutar la intalación falle el script `npm run instalar`, podemos ejecutar `npm install` **solamente** para instalar las dependencias. Una vez instalado lo ejecutaremos en modo desarrollo con `npm run dev` aunque es recomendable antes ejecutar las pruebas unitarias con `npm run test` que probarán conexiones con la base de datos entre otros, si una prueba falla probablemente se deba a la conexión con la base de datos en todo caso quizá podríamos seguir desarrollando con el proyecto pero las pruebas unitarias nos dan una idea de que puede estar fallando.

### Sobre la arquitectura

Este proyecto usa la arquitectura ddd y siguiendo este patrón de diseño en este proyecto se definen tres capas principales; infraestructura, dominio y aplicación [[16]](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/-/blob/master/doc/Arquitectura.md). En esta sesión y para crear un nuevo modelo necesitamos escribir sobre la capa de infraestructura. En sesiones posteriores escribiremos la lógica de negocios y crearemos endpoints para que un frontend pueda consultar/modificar los registros de la **nueva tabla posts** que crearemos, los endpoints y servicios se hacen en la capa de aplicación.

### Creando el esquema de la base de datos.

Aprovechando que este proyecto ya tiene definido un esquema de base de datos y todo un módulo de usuarios y permisos también definido, aprovecharemos algunas de esas tablas y relaciones.

![modulo-usuarios.png](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/-/raw/master/doc/img/modulo-usuarios.png)

Arriba las tablas del módulo usuarios y permisos de este proyecto base. Un diagrama de todas las tablas se puede revisar en [[17]](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/-/raw/master/doc/img/relaciones-bd.png) donde hay otras relaciones y tablas para guardar parámetros de configuración por ejemplo.

Como nuestra tabla posts no entra dentro del módulo definido como usuarios y permisos, vamos a tener que crear un nuevo módulo que para estas pruebas solo tendrá una sola tabla que se relaciona con otras tablas en otro módulo, la tabla se ve así:

![clase6-bd.jpeg](doc/imgs/clase6-bd.jpeg)

La tabla tiene el nombre `post_posts` para decir que pertenece al módulo **post**, esto para seguir la convención usada en este proyecto por ejemplo la tabla `usuario` en la BD se llama `sys_usuario` haciendo referencia al módulo **sys** el principal.

### Definiendo el modelo y las relaciones

Ahora crearemos el modelo creando una carpeta `src/infrastructure/models/post` donde pondríamos todos los modelos que pertenezcan a este nuevo módulo, en concreto el archivo `src/infrastructure/models/post/posts.js` tendrá el contenido:

```javascript
'use strict';

const lang = require('../../lang');
const util = require('../../lib/util');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: util.pk,
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      xlabel: lang.t('fields.create'),
      defaultValue: false
    },
    titulo: {
      type: DataTypes.STRING(500),
      allowNull: false,
      defaultValue: '-'
    },
    contenido: {
      type: DataTypes.STRING(1800),
      allowNull: false,
      defaultValue: '-'
    },
  };

  // Agregando campos para el log (esto agrega los campos _user_created _user_updated _created_at _updated_at)
  fields = util.setTimestamps(fields);

  let Posts = sequelize.define('posts', fields, {
    timestamps: false,
    tableName: 'post_posts'
  });

  return Posts;
};
```

En el modelo no definieremos las relaciones, en cambio lo haremos en un archivo dedicado a esto como lo hicimos en la [sesión 3](#sesión-3), modificaremos el archivo `src/infrastructure/associations.js` con las modificaciones:

```javascript
'use strict';

module.exports = function associations (models) {
  const {
    roles,
    usuarios,
    entidades,
    modulos,
    permisos,
    personas,
    tokens,
    posts     // importando el modelo
  } = models;

  // asociaciones de otras tablas
  // ...

  // Agregando asociaciones tabla posts
  posts.belongsTo(usuarios, { foreignKey: { name: 'id_usuario_autor', allowNull: false }, as: 'usuario_autor' });
  usuarios.hasMany(posts, { foreignKey: { name: 'id_usuario_autor', allowNull: false } });
  posts.belongsTo(usuarios, { foreignKey: { name: 'id_usuario_revisor', allowNull: true }, as: 'usuario_revisor' });
  usuarios.hasMany(posts, { foreignKey: { name: 'id_usuario_revisor', allowNull: true } });

  return models;
}
```

### Seeders para posts

Para terminar con la base de datos vamos a introducir algunos posts de pruebas. Para desarrollo posterior tendríamos que también agregar algunos registros para el módulo de **permisos** ya que esta aplicación da la facilidad de poner **restricciones** de lectura, escritura y otros por **roles**, pero haremos esa parte en la siguiente sesión para enfocarnos en la creación de modelos y la capa de infraestructura.

Entonces crearemos un nuevo archivo `src/infrastructure/seeders/0009-seeder-posts.js` con el contenido:

```javascript
'use strict';

const { setTimestampsSeeder } = require('../lib/util');
const { text } = require('../../common');

// nuevos registros
let items = [
  {
    visible: true,
    titulo: 'Post por defecto 1',
    contenido: 'Prueba de guarado de **post** usando seeders',
    id_usuario_autor: 2, // usuario ciudadano (ver 0004-seeder-usuarios.js)
    id_usuario_revisor: 1, // usuario ciudadano (ver 0004-seeder-usuarios.js)
  },
  {
    visible: false,
    titulo: 'Post por defecto 2',
    contenido: 'Prueba 2 de guarado de **post** usando seeders 0009-seeder-posts.js',
    id_usuario_autor: 2,
  }
];

// Asignando datos de log y timestamps a los datos
items = setTimestampsSeeder(items);

module.exports = {
  up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('post_posts', items, {});
  },

  down (queryInterface, Sequelize) { }
};
```

Finalmente para probar todos los cambios en la BD ejecutaremos:

```bash
npm run setup 
# si revisamos la base de datos en postgres se tendría que crear una nueva tabla post_posts
npm run seeders
# luego deberian existir registros en la tabla post_posts
```
### Agregando repositories

Siguiendo el modelo de arquitectura de este proyecto lo siguiente es crear *repositories* que son métodos que se encargan de interactuar directamente con los *models* y sirve como punto de acceso para capas superiores, aquí se manejan todas las funcionalidades del ORM (en este caso sequelize).

Vamos a crear una carpeta nueva `src/infrastructure/repositories/post` para nuestro módulo post y ahí escribiremos el archivo correspondiente `src/infrastructure/repositories/post/PostRepository.js`

```javascript
'use strict';

const { getQuery, toJSON } = require('../../lib/util');
const Repository = require('../Repository');

module.exports = function postRepository (models, Sequelize) {
  const { usuarios, modulos, posts } = models;

  async function findAll (params = {}) {
    let query = getQuery(params);

    query.where = {};
    // para incluir poder hacer consultas por campos de otras tablas
    query.include = [
      // de la tabla usuario
      {
        attributes: ['usuario', 'cargo', 'estado'],
        model: usuarios,
        as: 'usuario_autor'
      },
      { 
        attributes: ['usuario', 'cargo', 'estado'],
        model: usuarios,
        as: 'usuario_revisor'
      },
    ];

    if (params.visible !== undefined) {
      query.where.visible = params.visible;
    }

    // si se consulta por usuarios
    if (params.id_usuario_autor) {
      query.where.id_usuario_autor = params.id_usuario_autor;
    }
    if (params.id_usuario_revisor) {
      query.where.id_usuario_autor = params.id_usuario_revisor;
    }

    // ejecuta la funcion del ORM sequelize findAndCountAll
    const result = await posts.findAndCountAll(query);
    return toJSON(result);
  }

  async function findById (id) {
    const result = await posts.findOne({
      where: {
        id
      }
    });

    if (result) {
      return result.toJSON();
    }
    return null;
  }

  return {
    findAll,
    findById,
    createOrUpdate: (item, t) => Repository.createOrUpdate(item, posts, t),
    deleteItem: (id, t) => Repository.deleteItem(id, posts, t)
  };
};
```

La idea de los *repositories* es usar instrucciones para interactuar con la base de datos mediante el ORM y en lo posible no se debería incluir lógica de negocios por que para eso esta la capa de dominio. [[18]](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/-/blob/master/doc/Codigo.md)

### Escribiendo pruebas unitarias

Ahora probaremos la interacción con la base de datos que se hace a través de `PostRepository.js` y escribiremos [pruebas unitarias](https://es.wikipedia.org/wiki/Prueba_unitaria) dedicadas a este módulo que nos permitirán verificar que todo funciona como debería, este proyecto usa el framework para pruebas unitarias [ava](https://github.com/avajs/ava).

Entonces crearemos el archivo `src/infrastructure/tests/posts-tests.js`:

```javascript
'use strict';

const test = require('ava');
const { config, errors } = require('../../common');
const db = require('../');

let repositories;

test.beforeEach(async () => {
  if (!repositories) {
    repositories = await db(config.db).catch(errors.handleFatalError);
  }
});

test.serial('Post#findAll', async t => {
  // probando la lista de posts
  const { PostRepository } = repositories;
  let lista = await PostRepository.findAll();

  t.true(lista.count == 2, 'Se tienen dos posts');
});

test.serial('Post#findById', async t => {
  const { PostRepository } = repositories;
  const id = 1;

  let post = await PostRepository.findById(id);

  t.is(post.id, id, 'Se recuperó el registro mediante un id');
});

test.serial('Post#createOrUpdate - new', async t => {
  const { PostRepository } = repositories;
  
  const nuevoPost = {
    visible: true,
    titulo: 'Test post.',
    contenido: 'test content',
    id_usuario_autor: 2,
    id_usuario_revisor: 1, // usuario ciudadano (ver 0004-seeder-usuarios.js)
    _user_created: 1,
    _created_at: new Date()
  };

  let post = await PostRepository.createOrUpdate(nuevoPost);

  t.true(typeof post.id === 'number', 'Comprobando que el post tenga un id');
  t.is(post.titulo, nuevoPost.titulo, 'Creando registro - titulo');
  t.is(post.contenido, nuevoPost.contenido, 'Creando registro - contenido');
  t.is(post.visible, nuevoPost.visible, 'Creando registro - visible');

  test.idPost = post.id;
});

test.serial('Post#createOrUpdate - update', async t => {
  const { PostRepository } = repositories;
  
  const newData = {
    id: test.idPost,
    titulo: 'Cambiando titulo',
    contenido: 'Nuevo contenido de test',
  };

  let post = await PostRepository.createOrUpdate(newData);

  t.is(post.titulo, newData.titulo, 'Modificando registro - titulo');
  t.is(post.contenido, newData.contenido, 'Modificando registro - contenido');
});

test.serial('Post#findAll#filter#titulo', async t => {
  const { PostRepository } = repositories;
  let lista = await PostRepository.findAll({ titulo: 'Cambiando titulo' });

  t.true(lista.count >= 1, 'Se tiene 1 registros en la bd');
});

test.serial('Post#findAll#filter#visible', async t => {
  const { PostRepository } = repositories;
  let lista = await PostRepository.findAll({ visible: true });

  t.true(lista.count >= 2, 'Se tiene al menos 2 registros en la bd');
});

test.serial('Post#delete', async t => {
  const { PostRepository } = repositories;
  let deleted = await PostRepository.deleteItem(test.idPost);

  t.is(deleted, 1, 'Post eliminado');
});
```
El archivo tiene instrucciones que verifican el listado, búsqueda, creación, modificación y borrado de los posts, con esto probamos todo lo que hemos escrito en `PostRepository.js` y por ende la interacción con la BD de esta tabla. 

Para las pruebas podemos ejecutar `npm run test`, pero esto ejecutara todas las pruebas unitarias del proyecto asi que para acelerar las cosas solo ejecutaremos las pruebas de la capa de infraestrcutura y eso lo hacemos con:

```bash
npm run test-db
```

Al finalizar de ejecutar los tests se mostrará en la pantalla:

```
  ✔ posts-tests › Post#createOrUpdate - new
  ✔ posts-tests › Post#createOrUpdate - update
  ✔ posts-tests › Post#findAll#filter#titulo
  ✔ posts-tests › Post#findAll#filter#visible
  ✔ posts-tests › Post#delete
```

Que nos indica que todas las pruebas han terminado y se ha verificado correctamente el funcionamiento. Las pruebas unitaras son muy útiles por que a medida que vamos desarrollando nuevas funcionalidades o modificamos el proyecto podremos saber si algo ha dejado de funcionar por separado ejecutando las pruebas y por eso es importante escribir pruebas unitarias para todas las funcionalidades como sea posible.

> [video sesion backend 1](http://misc_publico.rmgss.net/varios/curso-node.js-vue.js-nucleognulinux-2020/sesion6-base-backend.mp4)

## Sesión 7

En esta sesión continuaremos con lo hecho en la sesión anterior para terminar de escribir el código necesario en el backend para poder crear *posts*, aprobarlos o rechazarlos exponiendo un servico api REST, para esto usaremos la capa de **dominio** y **aplicación**.

### domain/services

En la capa de dominio tenemos una subcarpeta `services` encargada de procesar las reglas de negocio a partir de datos obtenidos de la capa de infraestructura y el formato en que las funciones de esta capa devuelven la información es:

```
{
	"code": 1, // Donde: 1 es correcto, -1 es error y 0 es advertencia
	"data": "Datos de respuesta", // Se devuelve cadenas, números, objetos, etc.
	"message": "Mensaje personalizado"
}
```

#### Validadores

Antes de escribir el *services* para nuestros *posts* podemos escribir objetos de validación que nos ayudarán a validar que el objeto *post* que queremos crear/modificar sea válido haciendo una validación por campos y tipos. Crearemos varios archivos pequeños que hacen la validación de los campos propios:

`src/domain/value-objects/post/posts/PostVisible.js`:

```javascript
'use strict';

const Bool = require('../../general/Bool');

class PostVisible extends Bool {
  constructor (value, errors) {
    super('visible', value, { required: true }, errors);
  }
}

module.exports = PostVisible;
```

`src/domain/value-objects/post/posts/PostTitulo.js`:

```javascript
'use strict';

const Text = require('../../general/Text');

class PostTitulo extends Text {
  constructor (value, errors) {
    super('titulo', value, { required: true }, errors);
  }
}

module.exports = PostTitulo;
```

`src/domain/value-objects/post/posts/PostContenido.js`:

```javascript
'use strict';

const Text = require('../../general/Text');

class PostContenido extends Text {
  constructor (value, errors) {
    super('contenido', value, { required: true }, errors);
  }
}

module.exports = PostContenido;
```

Estos tres archivos nos servirán para usarlos como validadores de los campos `visible titulo contenido` tengan el tipo válido antes de hacer una modificación al registro en la base de datos. En el directorio `src/domain/value-objects/` hay una colección de validadores que podemos reutilizar para futuros desarrollos.

#### Archivo src/domain/services/post/PostsService.js

En este archivo escribiremos toda la lógica necesaria para creación, modificación y borrado de posts. A parte de las funciones básicas haremos una validación adicional:

* Un usuario solo puede crear hasta 3 posts no aprobados.

Entonces el archivo `src/domain/services/post/PostsService.js` tendrá:

```javascript
'use strict';

const debug = require('debug')('app:service:post');
const moment = require('moment');
const Service = require('../Service');

module.exports = function postService (repositories, valueObjects, res) {
  const { PostRepository, UsuarioRepository } = repositories; // importando de repositories
  const {
    PostVisible,
    PostTitulo,
    PostContenido
  } = valueObjects;  // validadores

  async function findAll(params = {}) {
    debug('Obteniendo Posts');
    let lista;

    try {
      lista = await PostRepository.findAll(params);
    } catch (e) {
      return res.error(e);
    }

    if (!lista) {
      return res.error(new Error(`Error al obtener la lista de posts`));
    }

    return res.success(lista);
  };

  async function findById(id) {
    debug('Buscar Post por id');
    let post;
    try {
      post = await PostRepository.findById(id);
    } catch (e) {
      return res.error(e);
    }

    return res.success(post);
  };

  async function createOrUpdate(data) {
    debug('Crear o actualizar post');

    validate(data);
    // verificando la cantidad de posts no aprobados
    if (!data.id) {
      try {
        let postsNoAprobados = await PostRepository.findAll({ visible: false, id_usuario_autor: data.id_usuario_autor });
        console.log('no aprobados:::', postsNoAprobados);
        if(postsNoAprobados.count > 3) {
          return res.error(`No se pueden crear mas de 3 posts sin aprobacion`);
        }
      } catch (e) {
        return res.error(`Error creando o actualizando post: ${e}`);
      }
    } else {
      delete data.id_usuario_autor;
    }
    // la funcion Service.createOrUpdate() llama al repository indicado y devuelve error o exito en formato
    return Service.createOrUpdate(data, PostRepository, res, 'Post');
  };

  async function deleteItem(id) {
    debug('Eliminar post');

    return Service.deleteItem(id, PostRepository, res, 'Post');
  }

  // fucion de validacion
  function validate(data) {
    Service.validate(data, {
      visible: PostVisible,
      titulo: PostTitulo,
      contenido: PostContenido
    });
  }

  return {
    findAll,
    findById,
    createOrUpdate,
    deleteItem
  };
};
```

Se utilizan las funciones escritas en el archivo `PostRepostory.js` en la sesión anterior, en el archivo nuevo `PostsService.js` podemos escribir todas las funciones que necesitemos usando lo previamente escrito en la capa infraestructura. En los *services* se puede incluir *repositories* de otros archivos sin restricción si hacen falta, por ejemplo si quiesieramos hacer una validación modificar registros de **otras** tablas.

En la función `createOrUpdate()` se ha gregado una verificacion que comprueba que el mismo usuario autor no pueda crear mas de tres *posts* sin aprobación en cuyo caso se devuelve un error con un mensaje.

#### Pruebas unitarias para *services*

Para tener la seguridad de que todo funciona como debería usaremos pruebas unitarias creando el archivo `src/domain/tests/posts-tests.js` con el contenido:

```javascript
'use strict';

const test = require('ava');
const { errors, config } = require('../../common');
const domain = require('../');

let services;

test.beforeEach(async () => {
  if (!services) {
    services = await domain(config.db).catch(errors.handleFatalError);
  }
});

test.serial('PostsService#findAll', async t => {
  const { PostsService } = services;
  let res = await PostsService.findAll();

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data.count == 2, 'Se tienen 2 posts');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('PostsService#findById', async t => {
  const { PostsService } = services;
  const id = 1;
  let res = await PostsService.findById(id);
  t.is(res.code, 1, 'Respuesta correcta');
  t.is(res.data.id, id, 'Se recuperó el registro mediante un id');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('PostsService#createOrUpdate', async t => {
  const { PostsService } = services;

  const nuevoPost = {
    visible: true,
    titulo: 'Test post.',
    contenido: 'test content',
    id_usuario_autor: 2,
    id_usuario_revisor: 1, // usuario ciudadano (ver 0004-seeder-usuarios.js)
    _user_created: 1,
    _created_at: new Date()
  };

  let res = await PostsService.createOrUpdate(nuevoPost);
  let post = res.data;

  t.is(res.code, 1, 'Respuesta correcta');
  t.true(typeof post.id === 'number', 'Comprobando que el nuevo post tenga un id');
  t.is(post.titulo, nuevoPost.titulo, 'Creando registro - titulo');
  t.is(post.visible, nuevoPost.visible, 'Creando registro - visible');
  t.is(post.contenido, nuevoPost.contenido, 'Creando registro - contenido');
  t.is(post.id_usuario_autor, nuevoPost.id_usuario_autor, 'Creando registro - id_usuario_autor');
  t.is(res.message, 'OK', 'Mensaje correcto');

  test.idPost = post.id;
});

test.serial('PostsService#createOrUpdate - update', async t => {
  const { PostsService } = services;
  const newData = {
    id: test.idPost,
    titulo: 'Nuevo Titulo cambiado test',
    visible: false
  };

  let res = await PostsService.createOrUpdate(newData);
  let post = res.data;
  t.is(res.code, 1, 'Respuesta correcta');
  t.is(post.id, newData.id, 'Actualizando registro post - id');
  t.is(post.titulo, newData.titulo, 'Actualizando registro post - titulo');
  t.is(post.visible, newData.visible, 'Actualizando registro post - visible');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('PostsService#delete', async t => {
  const { PostsService } = services;
  let res = await PostsService.deleteItem(test.idPost);
  t.is(res.code, 1, 'Respuesta correcta');
  t.true(res.data, 'Post eliminado');
  t.is(res.message, 'OK', 'Mensaje correcto');
});

test.serial('PostsService#createOrUpdate - crear mas de 3 posts no aprobados', async t => {
  const { PostsService } = services;

  const newData = [
    {
      visible: false,
      titulo: 'test post1',
      contenido: 'test content1',
      id_usuario_autor: 2,
      id_usuario_revisor: 1,
      _user_created: 1,
      _created_at: new Date()
    },
    {
      visible: false,
      titulo: 'test post2',
      contenido: 'test content2',
      id_usuario_autor: 2,
      id_usuario_revisor: 1,
      _user_created: 1,
      _created_at: new Date()
    },
    {
      visible: false,
      titulo: 'test post3',
      contenido: 'test content3',
      id_usuario_autor: 2,
      id_usuario_revisor: 1,
      _user_created: 1,
      _created_at: new Date()
    }
  ];
  let ids = [];
  // creando tres registros
  for (let i = 0; i < newData.length; i++) {
    let res = await PostsService.createOrUpdate(newData[i]);
    if (res.data.id) {
      ids.push(res.data.id);
    }
  }
  // tratando de crear un nuevo registro
  let newData4 = {
    visible: false,
    titulo: 'test post4',
    contenido: 'test content4',
    id_usuario_autor: 2,
    id_usuario_revisor: 1,
    _user_created: 1,
    _created_at: new Date()
  };
  let res = await PostsService.createOrUpdate(newData4);
  let post = res.data;

  t.is(res.code, -1, 'No se ha creado respuesta correcta');
  t.true(res.message != 'OK', 'No se ha creado');

  // borrando los registros creados de prueba
  for (let i = 0; i < ids.length; i++) {
    let res = await PostsService.deleteItem(ids[i]);
    t.is(res.code, 1, 'Respuesta correcta');
    t.true(res.data, 'Post eliminado');
    t.is(res.message, 'OK', 'Mensaje correcto');
  }
});
```

Este archivo tiene las pruebas básicas y una prueba especial que es cuando tratamos de crear mas de tres posts no aprobados para el mismo usuario y se verifica que no se permita esta creación. Una vez hecho esto se tiene que ejcutar nuevamente los seeders `npm run seeders` y luego ejecutar las pruebas unitaras:

```bash
npm run test-domain
```

### Exponiendo endpoint api rest

Ahora se tienen que trabajar en la capa de aplicación para exponer servicios api para la tabla `post_posts`, para esto son dos pasos básicos el primero crear un **controlador** y el siguiente una **ruta** que use ese controlador y exponga el servicio a través de un endpoint.

#### Controlador para posts

Crearemos el archivo `src/application/api/controllers/post/PostsController.js`:

```javascript
'use strict';

const debug = require('debug')('app:controller:post');
const { userData } = require('../../../lib/auth');
const moment = require('moment');

module.exports = function setupUsuarioController (services) {
  const { PostsService, UsuarioService } = services;

  async function crearPost (req, res, next) {
    debug('Creando post');
    const { titulo, contenido } = req.body;
    try {
      let _user = await userData(req, services);
      let respuesta = await PostsService.createOrUpdate({
        titulo,
        contenido,
        id_usuario_autor: _user.id,
        _user_created: _user.id,
        visible: false
      });
      return res.send(respuesta);
    } catch (e) {
      // usando next() de express y retornando error
      return next(e);
    }
  }

  async function listarPostsAdmin (req, res, next) {
    // un usuario admin puede ver todos los posts
    debug('listar posts');

    try {
      let respuesta = await PostsService.findAll();
      return res.send(respuesta);
    } catch (e) {
      return next(e);
    }
  }

  async function listarPostsUsuario (req, res, next) {
    // un usuario normal solo puede ver sus posts
    try {
      // comprobando usuario
      let _user = await userData(req, services);
      if (_user.id) {
        // consultando los posts solamente de ese usuario
        let respuesta = await PostsService.findAll({ id_usuario_autor: _user.id });
        return res.send(respuesta);
      }
    } catch(e) {
      return next(e);
    }
  }

  async function cambiarVisibilidadPost (req, res, next) {
    // se supone que esta funcion solo la puede llamar un usuario admin
    try {
      const { id } = req.params;
      const { visible } = req.body;
      let _user = await userData(req, services);
      let respuesta = await PostsService.createOrUpdate(
        {
          id: id, visible: visible,
          id_usuario_autor: _user.id
        });
      return res.send(respuesta);
    } catch (e) {
      return next(e);
    }
  };

  return {
    crearPost,
    listarPostsAdmin,
    listarPostsUsuario,
    cambiarVisibilidadPost,
  };
};
```

Este tiene la función de recibir las peticiones HTTP mediante `req` (request) y `res` (response) directamente de *express.js* y **usar** las funciones de la capa de dominio para procesarlas y retornar una respuesta a la petición HTTP original. En los controladores en lo posible se debe poner la menor cantidad de lógica ya que la capa de dominio tiene esa función principal.

Notarás que en caso de errores se usa el middleware `next()` de express.js para que la respuesta que sea manejada por otra función, en este caso la siguiente función *middleware* [[19]](https://expressjs.com/en/guide/using-middleware.html).

### definiendo endpoints

Se crea un nuevo archivo `api/routes/post/PostsRoute.js` con el contenido:

```javascript
'use strict';

const guard = require('express-jwt-permissions')();

module.exports = function setupPost (api, controllers) {
  const { PostsController } = controllers;
  // Los siguientes endpoints todos empiezan con /post/ (por el nombre de la carpeta en este mismo directorio)
  api.post('/crear', guard.check(['posts:create']), PostsController.crearPost);
  api.get('/listarPostsAdmin', guard.check(['posts:create', 'posts:read', 'posts:update']), PostsController.listarPostsAdmin);
  api.get('/listarPostsUsuario', guard.check(['posts:read']), PostsController.listarPostsUsuario);
  api.post('/cambiarVisiblidad/:id', guard.check(['posts:update']), PostsController.cambiarVisibilidadPost);

  return api;
};
```
Se puede ver como en ese archivo se definen los endpoints y los métodos HTTP a usar, también se usa la función `guard.check()` que recibe un array donde se especifican los permisos necesarios para acceder a ese endpoint, luego se pasa el controlador que atiende esa petición.

Para poder probar usaremos el base-frontend en la siguiente sesión.

> Todos los cambios realizados al base-backend se pueden revisar en el directorio [app6/base-backend](app6/base-backend)

> [video sesion backend 2](http://misc_publico.rmgss.net/varios/curso-node.js-vue.js-nucleognulinux-2020/sesion7-base-backend.mp4)

## Sesión 8

En esta sesión se agregarán los archivos necesarios como componentes vue y modificaciones para que el proyecto [base-frontend](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-frontend) muestre una interfaz que permita hacer la siguiente:

- Crear nuevos posts (rol: usuario)
- Revisar los posts y ponerlos como visibles (rol: admin)
- Mostrar la lista de posts y sus detalles (rol: usuario y admin)

### Instalación rápida

En el repositorio del [base-frontend](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-frontend) está el archivo `INSTALL.md` con instrucciones detalladas de instalación, a la fecha (14 Marzo de 2020) se puede resumir los siguientes pasos.

1. Descargar el proyecto.
2. Abrir una terminal en la carpeta del proyecto.
3. Asegurándose de usar node 10, instalamos las dependencias con `npm install`.
4. Crear los archivos `config/index.js config/dev.env.js` en base a sus respectivos .sample.
5. Ejecutar el proyecto en modo desarrollo con `npm run dev`.

### Sobre la estructura 

Como indica el archivo README.md del proyecto base-frontend la estructura se basa en una plantilla de webpack que se puede revisar en [http://vuejs-templates.github.io/webpack/structure.html](http://vuejs-templates.github.io/webpack/structure.html). Haremos la mayor parte de los cambios en la carpeta `src/components`

### Sobre la identificación de sesiones

El base-frontend envía al base-backend la identificación del usuario que ha ingresado (usuario actual) usando la cabecera *Authorization* con un *token* JWT (*json web token*) cifrado que el backend puede descifrar y luego identificar a qué sesión de usuario pertenece ese token. Por ejemplo la cabecera `Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c3VhcmlvIjoiYWRtaW4iLCJ...NjgyMjIzM30.wF2mnbVOKp4SZt7VrZOOXOwbL-nnyiT17kOINndQD6A` es un token *[Json web token](https://jwt.io/)*, que es un formato compacto de representación de *claims* para ser transmitidos como objetos json [[20]](https://tools.ietf.org/html/rfc7519#section-1).

Agregaremos en el archivo `data-auth.json` lo siguiente:

```javascript
    "menu": {
      "submenu": [
        // otros submenus
        // ...
        {
		    'url': 'post',
		    'label': 'Publicaciones (Posts)'
        }
       ]
    }
    "permisos": {
      // otros permisos
	  // ..
      "post:read": true,
      "post:create": true,
      "post:update": true,
      "post:delete": true,
    }
```

### Creando el nuevo módulo, habilitando el menú y definiendo Roles y permisos

Para introducir un registro de módulo en la base de datos se podría introducir nuevos registros en la tabla `sys_modulos` agregando el en archivo `src/infrastructure/seeders/0005-seeder-modulos.js` y `src/infrastructure/seeders/0006-seeder-permisos.js` del base-backend, pero esto es propenso a errores y para evitarlos usaremos la interfaz web del proyecto base-frontend, esto además nos permitirá ajsutar los permisos de manera conveniente.

Lo primero es ejecutar en otra terminal el proyecto [base-frontend](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-frontend) con `npm run dev`, luego ingresar al navegador http://localhost:8080 e ingresar como usuario **admin** y contraseña **123456**.

Para que el nuevo módulo aparezca como menú visible y se creen registros de este nuevo módulo en la base de datos correctamente, ingresaremos al panel de latera al menú "Módulos y permisos" y agregar.

![modulos](doc/imgs/clase8-1.png)

Luego llenamos el formulario con los datos mostrados en la imagen.

![creacion modulo](doc/imgs/clase8-2.png)

> Si hay un error a la hora de crear **y hasta que no se corrija en el repositorio principal** tendremos que hacer un parche agregando una línea en el archivo `src/components/admin/modulo/Modulo.vue` en el método `save()` mas o menos por la línea 411:

```javascript
        } else {
          data.estado = 'ACTIVO';
          // --- parche  ----
          data.orden = parseInt(data.orden);
		  // -------------
          this.$service.graphql({
            query: `
              mutation add($modulo: NewModulo!) {
                moduloAdd(modulo: $modulo) {
                  id
                  label
                }
              }
            `,
            variables: {
              modulo: data
            }
          }).then(response => {

```

Continunado al hacer guardar se debería mostrar un mensaje de éxito con el nuevo módulo creado, esto hará modificaciones en la tabla `sys_modulos` de la BD.

Luego definimos los permisos por roles para nuestro nuevo módulo de publicaciones (*posts*) ingresando al menú de módulos y permisos:

![menu permisos](doc/imgs/clase8-3.png)

Modificamos los permisos para que queden de la siguiente manera:

![menu roles permisos](doc/imgs/clase8-4.png)

Hecho esto la barra lateral se actualizará y aparecerá el menú de *Post* (Publicaciones).

![menu actualizado](doc/imgs/clase8-5.png)

### Creando componente para administracíon de posts

Crearemos el archivo `src/components/post/Post.vue`, donde se podrá listar los *posts* y marcarlos como visibles (solo administradores). Como este proyecto utiliza [veutify](https://vuetifyjs.com/) utilizaremos varios de sus componentes vue como [v-data-table](https://vuetifyjs.com/en/components/data-tables/#data-tables) para iterar sobre la lista de posts.

Este usará dos componentes que crearemos, uno para visualizar las publicaciones en un *modal* y el otro crear un nuevo post también un *modal*. 

```html
<template>
  <section>
    <h3 class="primary--text"><v-icon color="primary">mdi-card-text</v-icon> Publicaciones</h3>
    <v-card>
      <v-tooltip bottom>
        <v-btn slot="activator" color="primary"
               @click="openModalCrear()"><v-icon>add</v-icon> Crear post</v-btn>
        <span>Crear Post</span>
      </v-tooltip>
      <v-card-text>
        <!-- adminsitracion -->
        <v-data-table
          :headers="headers"
          :items="listaPosts"
          :loading="cargandoLista"
          :rows-per-page-items="[10,25,50]"
          :total-items="tamListaTotal"
        >
          <template slot="items" slot-scope="props">
            <td>
              <span>{{ props.item.usuario_autor.usuario }}</span>
            </td>
            <td>
              <span v-if="props.item.usuario_revisor">{{ props.item.usuario_revisor.usuario }}</span>
            </td>
            <td>
              <span>{{ props.item.titulo }}</span>
            </td>
            <td>
              <!-- cambiando segun permisos -->
              <span
                v-if="$store.state.permissions['posts:update']"
              >
                <v-switch
                  color="primary"
                  @change="cambiarVisiblidad(props.item)"
                  hide-details
                  class="mt-0"
                  v-model="props.item.visible"></v-switch>
              </span>
              <span v-else>
                <v-chip label color="success" text-color="white" v-if="props.item.visible">
                  SI
                </v-chip>
                <v-chip label color="warning" text-color="white" v-else>
                  NO
                </v-chip>
              </span>
            </td>
            <td>
              <small>{{ props.item.contenido.substring(0,50) + '...' }}</small>
              <p>
                <v-btn
                  color="primary"
                  @click.stop="openModal(props.item)"
                  slot="activator">
                  <v-icon>description</v-icon> ver
                </v-btn>
              </p>
            </td>
            <td>
              <span>{{ props.item._created_at }}</span>
            </td>
            <td>
              <span v-if="props.item._updated_at">{{ props.item._updated_at }}</span>
            </td>
          </template>
        </v-data-table>
        
        <!-- end administracion -->
      </v-card-text>
    </v-card>
    <!-- Modals-->
    <v-dialog v-model="$store.state.modal2" max-width="960">
      <post-visualizar :post="post"></post-visualizar>
    </v-dialog>
    <v-dialog v-model="$store.state.modal3" max-width="960">
      <post-crear></post-crear>
    </v-dialog>
  </section>
</template>

<script>
 import PostVisualizar from './PostVisualizar';
 import PostCrear from './PostCrear';
 export default {
   data () {
     return {
       // para la lista
       headers: [
         { text: 'Autor', sortable: false },
         { text: 'Revisor', sortable: false },
         { text: 'Título', sortable: true },
         { text: 'Visible', sortable: true },
         { text: 'Contenido', sortable: false },
         { text: 'Creación', sortable: true },
         { text: 'Modificación', sortable: false }
       ],
       cargandoLista: true,
       tamListaTotal: 0,
       tamPag: 10,
       numPag: 1,
       listaPosts: [],
       urlListarAdmin: 'post/listarPostsAdmin',
       urlListarUsuario: 'post/listarPostsUsuario',
       urlCambiarVisiblidad: 'post/cambiarVisiblidad',
       active: 'post',
       post: {}
     };
   },
   components: {
     PostVisualizar,
     PostCrear
   },
   methods: {
     cargarListaPosts () {
       var url = this.getUrlListar();
       this.$service.get(`${url}`).then(response => {
         if (response) {
           this.listaPosts = response.rows;
         }
       });
     },
     getUrlListar () {
       if (this.$store.state.permissions['posts:update']) {
         return this.urlListarAdmin;
       }
       return this.urlListarUsuario;
     },
     cambiarVisiblidad (post) {
       this.$service.post(`${this.urlCambiarVisiblidad}/${post.id}`, { visible: post.visible })
           .then(response => {
             console.log('respponse:', response);
             if (response.id) {
               this.$message.success('Se ha modificado el registro');
             } else {
               this.$message.error('No se pudo modificar el registro');
             }
           });
     },
     openModal (data = {}) {
       // Object.assign(this.post, data);
       this.post = data;
       this.$store.commit('openModal', 2);
     },
     openModalCrear () {
       this.$store.commit('openModal', 3);
     }
   },
   created () {
     this.$store.state.modal2 = false;
     this.$store.state.modal3 = false;
   },
   mounted () {
     this.$store.commit('closeModal', 2);
     this.$store.commit('closeModal', 3);
     this.cargarListaPosts();
   }
 };
</script>
```
Como se ve desde este componente se están llamando a otros dos, así se mantienen las funciones separadas y bien definidas, notarás que se usa un método `openModal()` que abre un nuevo *modal* cuyo contenido es el componente 

También para el *switch* para cambiar la visiblidad de un *post* se controla que el usuario actual tenga los permisos de modificación con `v-if="$store.state.permissions['posts:update']"` esto permite que un usuario con estos permisos tenga habilitado este *switch* y otros no.

### Creando componente vue para visualización de posts en detalle

Definieremos un nuevo componente para visualizar los posts con mayor comodidad aprovechando que podemos importar componentes, el componente será el archivo `src/components/post/PostVisualizar.vue`:

```html
<template>
  <v-card class="crud-dialog">
    <v-card-title class="headline m-0">
      <v-icon>description</v-icon> Publicación
      <v-spacer></v-spacer>
      <v-btn icon @click.native="$store.commit('closeModal', 2)">
        <v-icon>close</v-icon>
      </v-btn>
      <v-chip label color="success" text-color="white" v-if="post.visible">
        APROBADO
      </v-chip>
      <v-chip label color="warning" text-color="white" v-else>
        NO APROBADO o sin revisión
      </v-chip>
    </v-card-title>
    <v-card-text>
      <h3 v-if="post.titulo">{{ post.titulo }}</h3>
      <p>
        {{ post.contenido }}
      </p>
      <hr>
      <div>
        <!-- {{ post }} -->
        Creado por <strong>{{ post.usuario_autor.usuario }}</strong>
        el <strong>{{ $datetime.datetimeLiteral(post._created_at) }}</strong>
      </div>
      <div v-if="post.usuario_revisor">
        Revisado por: <strong>{{ post.usuario_revisor.usuario }}</strong>
      </div>
    </v-card-text>
    <v-card-actions>
      <v-spacer></v-spacer>
      <v-btn
        @click.native="$store.commit('closeModal', 2);">
        <v-icon>cancel</v-icon> {{$t('common.close') }}
      </v-btn>
    </v-card-actions>
  </v-card>
</template>

<script>
 export default {
   props: {
     post: {
       type: Object,
       default: () => {}
     }
   },
   data () {
     return {};
   }
 };
</script>
```

Tiene una sola propiedad que es el objeto con los datos del *post* que el componente que lo llama le proporciona. El botón para cerrar el *modal* que se abrió al llamar a este componente tiene la propiedad `@click.native="$store.commit('closeModal', 2);"` que escribe en el almacenamiento global con la instrucción de cerrar el modal 2. En el componente `Post.vue` se ha asignado el modal 2 para `PostVisualizar.vue`.

### Creando componente vue para creación de posts

Crearemos el archivo `src/components/post/PostCrear.vue` que es un componente con un formulario con dos campos para crear una nueva publicación.

```html
<template>
  <v-card class="crud-dialog">
    <v-card-title class="headline m-0">
      <v-icon>description</v-icon> Creación de publicación
      <v-spacer></v-spacer>
      <v-btn icon @click.native="$store.commit('closeModal', 3)">
        <v-icon>close</v-icon>
      </v-btn>
    </v-card-title>
    <v-card-text>
      <v-form
        @submit.prevent="crear">
        <v-card-text class="pt-0">
          <v-text-field
            v-model="datosEnviar.titulo"
            name="titulo"
            label="Título"
            maxlength="300"
            required>
          </v-text-field>
          <v-textarea
            v-model="datosEnviar.contenido"
            name="contenido"
            label="Contenido"
            clearable
            clear-icon="cancel"
          >
          </v-textarea>
        </v-card-text>
        <v-card-actions>
          <small class="error--text text-required">* Los campos son obligatorios</small>
          <v-spacer></v-spacer>
          <v-btn
            @click.native="$store.commit('closeModal', 3);">
            <v-icon>cancel</v-icon> {{$t('common.cancel') }}
          </v-btn>
          <v-btn
            color="primary"
            type="submit">
            <v-icon>check</v-icon> {{$t('common.save') }}
          </v-btn>
        </v-card-actions>
      </v-form>
    </v-card-text>
  </v-card>
</template>

<script>
 export default {
   props: {},
   data () {
     return {
       datosEnviar: {
         titulo: '',
         contenido: ''
       },
       urlCrear: 'post/crear'
     };
   },

   methods: {
     crear () {
       console.log('creando post', this.datosEnviar);
       this.$service.post(`${this.urlCrear}`, {
         titulo: this.datosEnviar.titulo,
         contenido: this.datosEnviar.contenido
       })
           .then(response => {
             if (response.id) {
               this.$message.success('Se ha creado el registro correctamente');
             } else {
               this.$message.error('No se pudo crear el registro');
             }
             this.$store.commit('closeModal', 3);
           });
     }
   }
 };
</script>
```

### Agregando el endpoint

Para que al entrar al endpoint `/posts` el frontend apunte a un componente modificaremos el archivo `src/router/index.js` donde se definen estos de forma global para que apunte al componente `src/components/post/Post.vue`, agregando las siguientes líneas:

```javascript
import Preferencias from '@/components/admin/preferencias/Preferencias';
import Log from '@/components/admin/Log';
// ---- importando el componente Post.vue
import Post from '@/components/post/Post';
// -----

// demás líneas del archivo
// ...
    
    {
      path: '/logs',
      name: 'Logs',
      component: Log
    },
	// -- agregando nuevo endpoint para los posts --
    {
      path: '/posts',
      name: 'Posts',
      component: Post
    },
	// ------------
    {
      path: '/404',
      component: AppNotFound
    },
	// ...
```

Con esos cambios ya podemos ver la interfaz completada y el botón del menú funcionando.

![posts](doc/imgs/clase8-6.png)
![posts2](doc/imgs/clase8-7.png)
![posts3](doc/imgs/clase8-8.png)

Las imágenes muestran la gestión básica de posts, todo se puede ir mejorando y ajustando gracias a las herramientas y flexibilidad que nos brindan estos proyectos base.

> Todos los cambios realizados al base-frontend se pueden revisar en el directorio [app6/base-frontend](app6/base-frontend)

> [video sesion frontend](http://misc_publico.rmgss.net/varios/curso-node.js-vue.js-nucleognulinux-2020/sesion8-base-frontend.mp4)

## Comentarios finales

Durante este breve curso se han repasado conceptos básicos de node.js, su funcionamiento y como interactúa con el mundo exterior mediante peticiones HTTP. También realizado la conexión a una base de datos mediante el ORM sequelize.

Luego se ha hecho una revisión introductoria a vue.js para escribir interfaces de usuario y aprovechar su funcionamiento básico. Para ello modificamos un proyecto estructurado y agregamos un módulo.

Finalmente utilizamos los proyectos [base-backend/](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/) y [base-frontend/](https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-frontend/) del repositorio estatal de software libre Boliviano. Estudiando la estructura y arquitectura de estos pudimos desarrollar un nuevo módulo de publicaciones y la posiblidad de administrarlos con una cuenta de usuario.

Hemos repasado los conceptos básicos y clave para continuar con el desarrollo, mejoras y utilizar estos proyectos base para construir nuevas plataformas web. 

Como estos proyectos son software libre, los cambios están permitidos y podemos beneficiarnos de todo ese conocimiento y experiencias, también hacer nuestras modificaciones y mejoras. Node.js y Vue.js son tecnologías que van constantemente mejorando. 

Con el campo libre para probar y desarrollar software con estos proyectos, tenemos abiertas nuevas posiblidades :D

## Referencias

1. https://nodejs.org/en/about/
2. https://github.com/nodejs/node/blob/master/BUILDING.md
3. https://github.com/nvm-sh/nvm#installing-and-updating
4. https://medium.com/the-node-js-collection/what-you-should-know-to-really-understand-the-node-js-event-loop-and-its-metrics-c4907b19da4c
5. http://www.catb.org/~esr/writings/taoup/html/ch01s06.html#id2877537
6. https://eloquentjavascript.net/10_modules.html#modules_npm
7. https://expressjs.com/en/starter/hello-world.html
8. https://sequelize.org/
9. https://sequelize.org/master/manual/model-basics.html
10. https://eloquentjavascript.net/08_error.html#h_zT3755/aOp
11. https://es.wikipedia.org/wiki/Patr%C3%B3n_de_dise%C3%B1o
12. https://vuejs.org/v2/guide/installation.html
13. https://vuejs.org/v2/guide/instance.html
14. https://vuejs.org/v2/guide/components.html
15. https://vuejsdevelopers.com/2017/03/24/vue-js-component-templates/
16. https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/-/blob/master/doc/Arquitectura.md
17. https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/-/raw/master/doc/img/relaciones-bd.png
18. https://gitlab.softwarelibre.gob.bo/agetic/agetic-aplicacion-base/base-backend/-/blob/master/doc/Codigo.md
19. https://expressjs.com/en/guide/using-middleware.html
20. https://tools.ietf.org/html/rfc7519#section-1
