'use strict';

module.exports = function associations (models) {
  const { lugares, carreteras, rutas } = models;

  // Representa: id_origen int references lugares(id),
  rutas.belongsTo(lugares, { foreignKey: { name: 'id_origen', allowNull: false }, as: 'lugar_origen' });
  lugares.hasMany(rutas, { foreignKey: { name: 'id_origen', allowNull: true }, as: 'lugar_origen' });

  // Representa: id_destino int references lugares(id),
  rutas.belongsTo(lugares, { foreignKey: { name: 'id_destino', allowNull: false }, as: 'lugar_destino' });
  lugares.hasMany(rutas, { foreignKey: { name: 'id_destino', allowNull: true }, as: 'lugar_destino' });

  // Representa: id_carretera int references carreteras(id)
  rutas.belongsTo(carreteras, { foreignKey: { name: 'id_carretera', allowNull: false }, as: 'carretera' });
  carreteras.hasMany(rutas, { foreignKey: { name: 'id_carretera', allowNull: true }, as: 'carretera' });

  return models;
};
