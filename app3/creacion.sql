create table lugares (
       id serial primary key not null,
       nombre varchar(100),
       descripcion varchar(5000),
       latitud decimal,
       longitud decimal
);

create table carreteras (
       id serial primary key not null,
       nombre varchar(200)
);

create table rutas (
       id serial primary key not null,
       salida varchar(20), -- para simplificar usaremos varchar en lugar de time
       llegada varchar(20), -- ...
       expedita boolean not null DEFAULT true,
       -- llaves foraneas
       id_origen int references lugares(id),
       id_destino int references lugares(id),
       id_carretera int references carreteras(id)
);

-- Valores iniciales de pruebas
insert into lugares (nombre, descripcion, latitud, longitud) values ('Villa Vilaque', 'Villa Vilaque frente', -16.4315, -68.3092);
insert into lugares (nombre, descripcion, latitud, longitud) values ('Santa Ana', '-', -16.4061, -68.3401);
insert into lugares (nombre, descripcion, latitud, longitud) values ('San Calixto', 'Área San Calixto', -16.2707, -68.4653);
insert into lugares (nombre, descripcion  , latitud, longitud) values ('Puerto Perez', 'Puerto Perez en Batallas', -16.3075, -68.5195);

insert into carreteras (nombre) values ('RN-2');
insert into carreteras (nombre) values ('Camino a Peñas');

insert into rutas (salida, llegada, expedita, id_origen, id_destino, id_carretera) values ('14/03/2020 14:04:29', '14/03/2020 14:35:11', true, 1, 2, 1);
insert into rutas (salida, llegada, expedita, id_origen, id_destino, id_carretera) values ('14/03/2020 14:36:09', '14/03/2020 14:45:11', true, 2, 3, 1);
insert into rutas (salida, llegada, expedita, id_origen, id_destino, id_carretera) values ('14/03/2020 14:50:09', '14/03/2020 15:20:01', true, 3, 4, 2);

insert into rutas (salida, llegada, expedita, id_origen, id_destino, id_carretera) values ('15/03/2020 19:12:05', '15/03/2020 19:45:11', true, 3, 4, 2);
insert into rutas (salida, llegada, expedita, id_origen, id_destino, id_carretera) values ('15/03/2020 20:30:31', '14/03/2020 20:45:46', true, 4, 3, 2);
