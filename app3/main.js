'use strict';

const { Sequelize } = require('sequelize');
const path = require('path');
const express = require('express');
const asyncify = require('express-asyncify');
const port = 3000;

const cors = require('cors');
const bodyParser = require('body-parser');

const db = require('./db');
const associations = require('./associations');

const configPostgres = {
  database: "pruebas1", // nombre de la BD
  dialect: "postgres",  // Dialecto postgres
  host: "localhost",    // host o direccion de la maquina
  password: "developer1",   // password
  username: "developer"  // nombre de usuario
};

const sequelize = new Sequelize(configPostgres);
const router = asyncify(express.Router());
const app = express();

function inicializarExpress() {
  try {
    // definiendo política CORS
    app.use(cors({
      origin: '*',
      methods: 'GET,POST',
      preflightContinue: false,
      headers: 'Cache-Control, Pragma, Content-Type, Authorization, Content-Length, X-Requested-With',
      'Access-Control-Allow-Headers': 'Authorization, Content-Type'
    }));
    // ajustando para poder recibir JSON
    app.use(bodyParser.json({limit: '10mb', extended: true, strict: false }));

    // para usar router en lugar de app.use
    router.use((req, res, next) => {
      Object.setPrototypeOf(req, app.request);
      Object.setPrototypeOf(res, app.response);
      req.res = res;
      res.req = req;
      next();
    });
    app.use('/', router);
  } catch (error) {
    console.error('Hubo un error al inicializar express,', error);
    throw new Error("Error inicializando express:" + error);
  }
}

async function inicializarBD() {
  // probando conexion
  try {
    await sequelize.authenticate(); 
    console.log("✓ Conectado satisfactoriamente");
  } catch(error) {
    console.error("Error al conectarse a la BD:", error);
    throw new Error("weeeeeeeeeeee", error);
  }
  // cargando modelos
  let _models;
  try {
    _models = db.loadModels(path.join(__dirname, 'models'), sequelize);
    console.log('✓ Modelos cargados exitosamente.');
  } catch (error) {
    console.error('✕ Error cargando modelos', error);
    throw new Error("Error cargando modelos en la BD:", error);
  }
  // cargando asociaciones
  let models;
  try {
    models = associations(_models);
    console.log('✓ Asociaciones cargadas exitosamente.');
  } catch (error) {
    console.error('✕ Error cargando asociaciones', error);
    throw new Error("Error cargando asociaciones en la BD:", error);
  }

  console.log(' *** Modelos cargados ***');
  console.log(models);
  return models;
}

async function definirEndpoints(models) {
  router.get('/', (req, res) => {
    res.status(200).send("Hola");
  });

  // endpoint para listar lugares
  router.get('/lugares', async (req, res) => {
    const { lugares } = models;
    let respuesta = await lugares.findAndCountAll({});
    res.status(200).send({ lugares: respuesta });
  });

  // endpoint introducir lugares
  router.post('/add-lugar', async (req, res) => {
    const { lugares } = models;
    const { lugar } = req.body;
    console.log('Recibido:', lugar);
    let resultado = await lugares.create(lugar);
    res.status(200).send(resultado);
  });

  router.get('/lugares-nombre/:nombre', async (req, res) => {
    // tarea: Retornar solo los lugares que tengan el :nombre
    const { nombre } = req.params;
    // ... consutlar aqui
    // res.status(200).send({ lugares: ... })
  });
}

async function main() {
  try {
    inicializarExpress();
  } catch (error) {
    console.error('Error iniciando express:', error);
    process.exit(1);
  }

  let models;
  try {
    models = await inicializarBD();
  } catch (error) {
    console.error('Error iniciando BD:', error);
    process.exit(1);
  }

  try {
    definirEndpoints(models);
  } catch (error) {
    console.error('Error al definir endpoints:', error);
    process.exit(1);
  }

  app.listen(port, () => console.log(`Aplicación escuchando en puerto: ${port}!`));
}

main();
