'use strict';

const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER,
      xlabel: 'ID'
    },
    nombre: {
      type: DataTypes.STRING(200),
      defaultValue: 'No nombrada',
      allowNull: false
    }
  };

  let Carreteras = sequelize.define('carreteras', fields, {
    timestamps: false,
    tableName: 'carreteras'
  });

  return Carreteras;
};

