'use strict';

const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  let fields = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER,
      xlabel: 'ID'
    },
    salida: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: '01/01/1980 00:00:00', // Formato DD/MM/AAAA HH:MM:SS      
    },
    llegada: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: '01/01/1980 00:00:00', // Formato DD/MM/AAAA HH:MM:SS      
    },
    expedita: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false
    }
  };

  let Rutas = sequelize.define('rutas', fields, {
    timestamps: false,
    tableName: 'rutas'
  });

  return Rutas;
};
