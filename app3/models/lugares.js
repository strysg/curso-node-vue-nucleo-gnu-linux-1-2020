'use strict';

const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  // definimos las columnas de la tabla lugares
  let fields = {
    id: {
      primaryKey: true,
      autoIncrement: true,
      type: Sequelize.INTEGER,
      xlabel: 'ID'
    },
    nombre: {
      type: DataTypes.STRING(100),
      defaultValue: 'Sin nombre',
      allowNull: false
    },
    descripcion: {
      type: DataTypes.STRING(5000),
      defaultValue: 'Sin descripción',
      allowNull: false
    },
    latitud: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    longitud: {
      type: DataTypes.DECIMAL,
      allowNull: false
    }
  };

  let Lugares = sequelize.define('lugares', fields, {
    timestamps: false,
    tableName: 'lugares'
  });

  return Lugares;
};

