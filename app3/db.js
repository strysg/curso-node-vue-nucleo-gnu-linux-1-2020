'use strict';

const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');

// algunos utilitarios
function removeAll (elements, list) {
  var ind;

  for (var i = 0, l = elements.length; i < l; i++) {
    while ((ind = list.indexOf(elements[i])) > -1) {
      list.splice(ind, 1);
    }
  }
}

/**
 * Carga los modelos de la carpeta especificada
 *
 * @param {string} PATH: Path del directorio de donde se cargará los modelos del sistema
 * @param {object} sequelize: Objeto Sequelize
 */
function loadModels (PATH, sequelize) {
  let files = fs.readdirSync(PATH);
  let models = {};

  // por cada archivo obtenido se carga un modelo (volviendo a llamar a esta misma funcion)
  files.forEach(function (file) {
    let pathFile = path.join(PATH, file);
    if (fs.statSync(pathFile).isDirectory()) {
      models[file] = loadModels(pathFile, sequelize);
    } else {
      file = file.replace('.js', '');
      models[file] = sequelize.import(pathFile);
    }
  });

  return models;
}

// campos extra para guardar la fecha de creacion/modificacion (opcional)
const timestamps = {
  _created_at: {
    type: Sequelize.DATE,
    allowNull: false,
    label: '_created_at',
    defaultValue: Sequelize.NOW
  },
  _updated_at: {
    type: Sequelize.DATE,
    xlabel: 'fields._updated_at'
  }
};

function setTimestamps (fields) {
  return Object.assign(fields, timestamps);
}

module.exports = {
  loadModels,
  setTimestamps
};
