const adivinanzas = require('./adivinanzas');

let puntuacionActual = {
  restantes: 0,
  codigo: '',
  maxIntentos: 0,
  palabra: '',
  palabraActual: ''
};

const nuevaAdivinanza = () => {
  let adivinanza = adivinanzas[parseInt(Math.random()*adivinanzas.length)];
  puntuacionActual.restantes = adivinanza.maxIntentos;
  puntuacionActual.maxIntentos = adivinanza.maxIntentos;
  puntuacionActual.codigo = adivinanza.codigo;
  puntuacionActual.palabra = adivinanza.palabra;
  puntuacionActual.palabraActual = adivinanza.fprevia;
  return {
    palabraActual: puntuacionActual.palabraActual,
    codigo: puntuacionActual.codigo,
    restantes: puntuacionActual.restantes
  };
};

// obtiene todos los indeces de `val' en `arr'
// Fuente: https://stackoverflow.com/questions/20798477/how-to-find-index-of-all-occurrences-of-element-in-array#20798567
function getAllIndexes(arr, val) {
    var indexes = [], i = -1;
    while ((i = arr.indexOf(val, i+1)) != -1){
        indexes.push(i);
    }
    return indexes;
}

// Agregando funcion para reemplazar una cadena en un indice dado, en javascript las cadenas son inmutables
// Fuente: https://stackoverflow.com/questions/1431094/how-do-i-replace-a-character-at-a-particular-index-in-javascript
String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}

const verificarLetra = (letra) => {
  let indices = getAllIndexes(puntuacionActual.palabra, letra);
  indices.forEach(i => {
    console.log('>', puntuacionActual.palabraActual.replaceAt(i, puntuacionActual.palabra[i]));
    puntuacionActual.palabraActual  = puntuacionActual.palabraActual.replaceAt(i, puntuacionActual.palabra[i]);
  });
  return indices.length;
};

const intento = (codigo, letra) => {
  if (puntuacionActual.restantes == 0) {
    return {
      error: 'No hay intentos restantes',
      puntuacionActual: {
        palabraActual: puntuacionActual.palabraActual,
        codigo: puntuacionActual.codigo,
        restantes: puntuacionActual.restantes
      }
    };
  }
  if (codigo !== puntuacionActual.codigo) {
    return {
      error: 'Inválido',
      puntuacionActual: {
        palabraActual: puntuacionActual.palabraActual,
        codigo: puntuacionActual.codigo,
        restantes: puntuacionActual.restantes
      }
    };
  }
  puntuacionActual.restantes = puntuacionActual.restantes - 1;
  verificarLetra(letra);
  return {
    puntuacionActual: {
      palabraActual: puntuacionActual.palabraActual,
      codigo: puntuacionActual.codigo,
      restantes: puntuacionActual.restantes
    }
  };
};

const getPuntuacionActual = () => {
  return puntuacionActual;
};

module.exports = {
  nuevaAdivinanza,
  intento,
  getPuntuacionActual
};
