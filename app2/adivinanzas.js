const adivinanzas = [
  {
    palabra: "conciencia",
    fprevia: "c  c  e  a",
    maxIntentos: 10,
    codigo: 'CU&wx31w'
  },
  {
    palabra: "comunicación",
    fprevia: "  mu  c  i n",
    maxIntentos: 10,
    codigo: '996143912551'
  },
  {
    palabra: "características",
    fprevia: " a  c  r   i",
    maxIntentos: 10,
    codigo: 'n2f551sca'
  },
  {
    palabra: "pragmáticamente",
    fprevia: "p           nte",
    maxIntentos: 15,
    codigo: 'p-3C1lw8541'
  },
  {
    palabra: "código",
    fprevia: "  d  o",
    maxIntentos: 10,
    codigo: 'F85[-1cff'
  },
];

module.exports = adivinanzas;

