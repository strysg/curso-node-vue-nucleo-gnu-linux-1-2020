'use strict';

const express = require('express');
// aqui creamos la instancia
const app = express();
const port  = 3000;

const cors = require('cors');
const bodyParser = require('body-parser');

const utils = require('./utils');

// definiendo política CORS
app.use(cors({
  origin: '*',
  methods: 'GET,POST',
  preflightContinue: false,
  headers: 'Cache-Control, Pragma, Content-Type, Authorization, Content-Length, X-Requested-With',
  'Access-Control-Allow-Headers': 'Authorization, Content-Type'
}));

// Ajustando para que la aplicación express acepte y pueda analizar mensajes tipo JSON
app.use(bodyParser.json({limit: '10mb', extended: true, strict: false }));

// definimos los endpoints
app.get('/', (req, res) => {
  res.send('¡Hola mundo!');
});

// endpoint inicio para empezar un nuevo intento
app.get('/nuevo', (req, res) => {
  // seleccionando palabra para adivinar
  return res.status(200).send(utils.nuevaAdivinanza());
});

// endpoint para verificar letra
app.post('/letra', (req, res) => {
  // obteniendo valores enviados en el cuerpo json de la peticion
  const { codigo, letra } = req.body;
  if (codigo.length == 0 || letra.length == 0) {
    return res.status(200).send({ error: 'Debe enviar una letra'});
  }
  let resultadoIntento = utils.intento(codigo, letra);
  console.log('----', resultadoIntento, '\n----');
  if (resultadoIntento.error) {
    return res.status(200).send(resultadoIntento);
  }
  if (resultadoIntento.puntuacionActual.palabraActual.indexOf(' ') == -1) {
    // si no hay  espacios en blanco significa que encontró todas las letras
    return res.status(200).send({
      finalizado: true,
      puntuacionActual: resultadoIntento.puntuacionActual
    });
  }
  // enviando respuesta con status code 200
  return res.status(200).send(resultadoIntento);
});

// poniendo la aplicación en modo escucha en el puerto 3000
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
